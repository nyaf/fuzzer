#include <stdio.h>
#include <string.h>

char *serial = "\x31\x3e\x3d\x26\x31";

void win(){
  printf("You win!\n");

  // Let's crash something
  while(1);
}

int check(char *ptr, int len)
{
  int i = 0;
  while (i < 5){
    if (((ptr[i] - 1) ^ 0x55) != serial[i])
      return 1;
    i++;
  }
  return 0;
}

int main(int argc, char **argv) {
  if(argc != 2) {
    printf("Bad args\n");
    return 1;
  }

  if(check(argv[1], strlen(argv[1])))
    printf("Bad pass: %s\n", argv[1]);
  else
    win();

  return 0;
}

