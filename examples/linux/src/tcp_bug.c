#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>


char *serial = "\x31\x3e\x3d\x26\x31";

int check(char *ptr, int len)
{
  int i = 0;
  while (i < 5){
    if (((ptr[i] - 1) ^ 0x55) != serial[i])
      return 1;
    i++;
  }

  *(int*)0 = 0;
}

int main(int argc, char *argv[])
{
	int listenfd = 0, connfd = 0;
	struct sockaddr_in serv_addr;

	char sendBuff[1025];
	time_t ticks;

	/* creates an UN-named socket inside the kernel and returns
	 * an integer known as socket descriptor
	 * This function takes domain/family as its first argument.
	 * For Internet family of IPv4 addresses we use AF_INET
	 */
	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	memset(&serv_addr, '0', sizeof(serv_addr));
	memset(sendBuff, '0', sizeof(sendBuff));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(atoi(argv[1]));

	/* The call to the function "bind()" assigns the details specified
	 * in the structure 『serv_addr' to the socket created in the step above
	 */
	bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

	/* The call to the function "listen()" with second argument as 10 specifies
	 * maximum number of client connections that server will queue for this listening
	 * socket.
	 */
	listen(listenfd, 10);

  /* In the call to accept(), the server is put to sleep and when for an incoming
   * client request, the three way TCP handshake* is complete, the function accept()
   * wakes up and returns the socket descriptor representing the client socket.
   */
  connfd = accept(listenfd, (struct sockaddr*)NULL, NULL);

  /* As soon as server gets a request from client, it prepares the date and time and
   * writes on the client socket through the descriptor returned by accept()
   */
  snprintf(sendBuff, sizeof(sendBuff), "Password: ");
  write(connfd, sendBuff, strlen(sendBuff));

  int n = 0;
  char recvBuff[1024];
  memset(recvBuff, '0',sizeof(recvBuff));

  n = read(connfd, recvBuff, sizeof(recvBuff)-1);
  recvBuff[n] = 0;
  fputs(recvBuff, stdout);

  check(recvBuff, sizeof(recvBuff));
  printf("Bad pass\n");
  write(connfd, "Bad pass\n", 9);
  close(connfd);
}

