#include <stdio.h>
#include <stdlib.h>

int main() {
  /* Initial memory allocation */
  char *buff = (char *)malloc(150);

  for (unsigned int i = 0; i < 70; i++)
    buff[i] = 0;

  free(buff);

  return buff[100];
}
