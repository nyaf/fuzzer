#include <stdio.h>
#include <string.h>

char *serial = "\x31\x3e\x3d\x26\x31";

void check(char *input)
{
  int cnt = 0;

  if (input[0] == 'b') cnt++;

  if (input[1] == 'a') cnt++;

  if (input[2] == 'd') cnt++;

  if (input[3] == '!') cnt++;

  if (cnt == 4)
    return *(int*)0 = 0;
}

int main(int argc, char **argv) {
  if(argc != 2) {
    printf("Bad args\n");
    return 1;
  }

  check(argv[1]);

  printf("Bad pass: %s\n", argv[1]);

  return 0;
}

