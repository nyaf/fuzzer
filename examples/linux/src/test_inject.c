#include <stdio.h>

void vuln(char* str, int size)
{
  printf(str, size);
}

int main(int argc, void **argv) {
  char buffer[20];

  printf("[+] Before inject\n");

  vuln("NOPASONA %d\n", 9);

  printf("[+] After inject\n");
}
