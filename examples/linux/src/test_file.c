#include <stdio.h>

int main(int argc, char **argv) {
  if(argc != 2) {
    printf("Bad args\n");
    return 1;
  }

  FILE* file = fopen(argv[1], "r");

  if(file == NULL) {
    printf("File not exists: '%s' . error: %p\n", argv[1], file);
    return 2;
  }

  char c = fgetc(file);
  while (c != EOF)
  {
    printf ("%c", c);
    c = fgetc(file);
  }
}
