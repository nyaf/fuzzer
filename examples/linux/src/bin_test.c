#include <stdio.h>

int main(int argc, void **argv) {
  char buffer[20];

  printf("[+] Before read\n");

  fgets(buffer, 30, stdin);
  printf(buffer);

  printf("[+] After read\n");
}
