#include <stdio.h>
#include <string.h>

char *serial = "\x31\x3e\x3d\x26\x31";

void win(){
  printf("You win!\n");

  // Let's crash something
  *(int*)0 = 0;
}

int check(char *ptr)
{
  int i = 0;
  while (ptr[i] != '\0'){
    if (((ptr[i] - 1) ^ 0x55) != serial[i])
      return 0;
    i++;
  }
  return i == 5;
}

int main(int argc, char **argv) {
  if(argc != 2) {
    printf("Bad args\n");
    return 1;
  }

  if(check(argv[1]))
    win();
  else
    printf("Bad pass: %s\n", argv[1]);

  return 0;
}

