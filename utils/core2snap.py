#!/bin/python

import os
import pickle
from argparse import ArgumentParser
from pathlib import Path
from pprint import pprint
from typing import Dict, List, Optional, Tuple

import lief
from lief.ELF import SEGMENT_FLAGS, Binary, CorePrStatus

EXCL_REGS_LIST: List[str] = [""]
CONV_REGS_TABLE: Dict[str, str] = {"x31": "sp"}


def get_core_registers(core: Binary) -> Optional[Dict]:
    regs_note: Optional[CorePrStatus] = None

    for note in core.notes:
        if note.type_core == lief.ELF.NOTE_TYPES_CORE.PRSTATUS:
            regs_note = note.details
            break

    if not regs_note:
        print("[X] Can't found registers note in the core dump.")
        return None

    return regs_note.register_context


def core_regs_to_snap(core_regs: Dict) -> Dict:
    snap_regs: Dict[str, int] = {}

    for key, elem in core_regs.items():
        reg_name: str = key.name.split("_")[-1].lower()

        if reg_name in EXCL_REGS_LIST:
            continue

        if reg_name in CONV_REGS_TABLE:
            reg_name = CONV_REGS_TABLE[reg_name]

        snap_regs[reg_name] = elem

    return snap_regs


def search_path(file_entries: List, start_address: int) -> str:
    for file in file_entries:
        if file.start == start_address:
            return file.path

    return ""


def convert_prot_bits(flags: SEGMENT_FLAGS) -> int:
    flag_value: int = int(flags)
    res: int = 0

    # EXEC
    if flag_value & SEGMENT_FLAGS.X:
        res |= 4

    # WRITE
    if flag_value & SEGMENT_FLAGS.W:
        res |= 2

    # READ
    if flag_value & SEGMENT_FLAGS.R:
        res |= 1

    return res


def get_core_memory(core: Binary) -> List[Tuple[int, int, int, str, bytes]]:
    snap_mem: List[Tuple[int, int, int, str, bytes]] = []

    for note in core.notes:
        if note.type_core == lief.ELF.NOTE_TYPES_CORE.FILE:
            files_note = note.details
            break

    file_entries = list(files_note)

    for segment in core.segments:
        if segment.virtual_address == 0:
            continue

        path: str = search_path(file_entries, segment.virtual_address)

        snap_mem.append(
            (
                segment.virtual_address,
                segment.virtual_address + segment.virtual_size,
                convert_prot_bits(segment.flags),
                path,
                bytes(segment.content),
            )
        )

    return snap_mem


def save_snapshot(snapshot: Dict, filename: str) -> None:
    with open(filename, "wb") as snap_file:
        pickle.dump(snapshot, snap_file)


def convert(input: str, output: str) -> bool:
    snapshot: Dict = {}

    core: Binary = lief.parse(input)

    core_regs = get_core_registers(core)

    print(f"[+] Registers found ({len(core_regs)}):")
    pprint(core_regs)

    snapshot["reg"] = core_regs_to_snap(core_regs)

    print(f"[+] Registers converted ({len(snapshot['reg'])}):")
    pprint(snapshot["reg"])

    core_memory = get_core_memory(core)
    print("[+] Memory mapping found:")
    for item in core_memory:
        pprint(item[:-1])

    snapshot["mem"] = {"ram": core_memory}
    snapshot["mem"]["mmio"] = []

    save_snapshot(snapshot, output)

    return True


def parse_args() -> Tuple[int, int, str]:
    parser = ArgumentParser(
        prog="core2snap",
        description="NYAF core dump to qiling snapshot conversion tool.",
    )

    parser.add_argument("input", type=str, help="Core dump input file.")
    parser.add_argument("output", type=str, help="Qiling snapshot output file.")

    args = parser.parse_args()

    return args.input, args.output


def main() -> int:
    print("Conversion initializing")

    input: str
    output: str

    input, output = parse_args()

    if not os.path.isfile(input):
        print("[X] Bad input file.")
        return 1

    if not os.path.isdir(Path(output).parent.absolute()):
        print("[X] Bad output file.")
        return 2

    print(f"[*] Convertig {input} and saving in {output}.")

    if not convert(input, output):
        print("[X] Conversion fail.")
        return 3

    return 0


if __name__ == "__main__":
    exit(main())
