#!/bin/python

from argparse import ArgumentParser
from os import path
from pathlib import Path
from subprocess import DEVNULL, check_output, run
from typing import List, Optional, Tuple


def generate_core_dump(pid: int, address: int, output: str) -> None:
    gdb_script: List[str] = [
        f"br *{address}",
        "c",
        f"generate-core-file {output}",
        "detach",
        "quit",
    ]

    cmd: List[str] = [
        "gdb",
        "-p",
        str(pid),
    ]

    for c in gdb_script:
        cmd.append("-ex")
        cmd.append(c)

    print(f"[*] Running {cmd}")

    run(cmd, stdout=DEVNULL, stderr=DEVNULL)


def get_proc_base(pid: int) -> Optional[int]:
    cmd: List[str] = ["cat", f"/proc/{pid}/maps"]

    output: str
    try:
        output = check_output(cmd)
    except Exception:
        return None

    for line in output.splitlines():
        if b"/" in line and b"lib" not in line and b"[" not in line:
            return int("0x" + line.decode().split("-")[0], 16)

    return None


def process_exists(pid: int) -> bool:
    return path.isdir(f"/proc/{pid}/")


def parent_isdir(filename: str) -> bool:
    file_path = Path(filename)
    parent: str = file_path.parent.absolute()

    return path.isdir(parent)


def parse_args() -> Tuple[int, int, str]:
    parser = ArgumentParser(prog="dummper", description="NYAF process dumper tool.")

    parser.add_argument("pid", type=int, help="pid of the target process (int).")
    parser.add_argument(
        "address", type=str, help="Address where to take the dump (hex)."
    )
    parser.add_argument("output", type=str, help="Filename for the generated dump.")

    args = parser.parse_args()

    return args.pid, args.address, args.output


def main() -> None:
    print("Preparing to generate the core dump.")
    pid: int
    address: int
    hex_address: str
    filename: str

    pid, hex_address, filename = parse_args()

    try:
        address = int(hex_address, 16)
    except Exception:
        print("[X] Bad address")
        return 1

    print(f"[*] Target pid: {pid}")
    print(f"[*] Target address: {hex(address)}")

    if not process_exists(pid):
        print("[X] Process not exists")
        return 2

    if not parent_isdir(filename):
        print("[X] Invalid output filename")
        return 3

    base_address: Optional[int] = get_proc_base(pid)

    if base_address is None:
        print("[X] Can't found process base address")
        return 4

    print(f"[*] Process base address: {hex(base_address)}")

    generate_core_dump(pid, address + base_address, filename)

    if not path.isfile(filename):
        print("[X] Error creating the core file.")
        return 5

    print(f"[+] Core dump saved in {filename}")
    print("Bye...")

    return 0


if __name__ == "__main__":
    main()
