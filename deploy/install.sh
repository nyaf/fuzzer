#!/bin/bash

# Check Root
if [[ $(id -u) -ne 0 ]];
  then echo "Please run as root"
  exit 1
fi

# Uninstall
if [[ $1 = "uninstall" ]]; then
  rm -fr /opt/nyaf
  exit 0
fi

# Go to the root
if [[ "$PWD" == */deploy ]] ; then
  cd ..
fi

# Check if is installed
if [[ -d "/opt/nyaf/" ]]
then
    echo "NYAF is already installed. Exiting..."
    exit 1
fi

# Install dependencies
echo "Installing dependencies..."
mkdir /opt/nyaf
python3 -m pip install virtualenv
python3 -m virtualenv /opt/nyaf/venv
source /opt/nyaf/venv/local/bin/activate
python3 -m pip install -r requirements.txt
deactivate

echo "Installing nyaf..."
# Copy src code
cp -r ./src/* /opt/nyaf
# Install the binary
cp ./nyaf /usr/bin/
