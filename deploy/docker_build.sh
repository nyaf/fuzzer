#!/bin/bash

if [[ "$PWD" == */deploy ]] ; then
  cd ..
fi

docker build -t nyaf -f ./deploy/Dockerfile .
