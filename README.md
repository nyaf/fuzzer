# NYAF: Not Yet Another Fuzzer (WIP) #

__*Documentation in progress*__

A multi-platform fuzzer based on emulation and guided by the Generation Search algorithm, seen for the first time on the well known [SAGE](https://patricegodefroid.github.io/public_psfiles/cacm2012.pdf) fuzzer.

The main goal of the fuzzer is to abstract the program from its natural execution environment (OS/Hardware), and analyze it in a more comfortable and emulated one, to improve the capabilities in terms of analysis, scalability and efficiency. This is a really great approach working on targets like IOT devices, embedded systems, firmwares and OS.

Currently, the fuzzer supports the following architectures and platforms:

* Architectures:
  * ARM32
  * AArch64
  * X86
  * x86_64

* Platforms:
  * Windows
  * MacOs
  * Linux
  * UEFI
  * Bare metal

NYAF is fully written in python, so it can be easily extended to improve its functionalities, provide support for another platform or create a custom test case generation method. It primary uses two python frameworks to work:

• [Qiling](https://github.com/qilingframework/qiling): an advanced binary emulation framework built on top of [Unicorn Engine](https://github.com/unicorn-engine/unicorn) in pure python used to emulate the programs.

• [Triton](https://triton-library.github.io/): A Dynamic Binary Analysis framework used to implement the concolic execution needed in the General Search algorithm.

# Get Started #

## Installation ##

### With Docker (Recommended) ###

__Requirements:__
* [Docker](https://docs.docker.com/get-docker/)

```bash
./docker_nyaf [ rootfs directory ] [ config file ]
```

### Local Installation ###

__Requirements:__
* python3
* virtualenv
* Triton
* Capstone


```bash
# Install
sudo ./deploy/install.sh

# Run
nyaf [ rootfs directory ] [ config file ]
```

### Local Run ###

__Requirements:__
* python3
* virtualenv

```bash
# Install python3 (Ubuntu)
sudo apt install python3

# Install python virtualenv
python3 -m pip install virtualenv

# Create a virutalenv
python3 -m virtualenv venv
source ./venv/bin/activate

# Install dependencies
python3 -m pip install -r requirements.txt

# Run
./nyaf [ rootfs directory ] [ config file ]
```


## Example ##

We are going to run the fuzzer on the target over a linux AArch64 binary and using the docker version.
The only requirement for this example is [Docker](https://docs.docker.com/engine/install/).

* (Target source)[/examples/linux/src/elite_bug.c]
* (Target binary)[/examples/linux/arm64/elite_bug]
* (Fuzz config)[/examples/linux/arm64/elite_bug.yml]

```bash
# Clone this repo
$ git clone https://gitlab.com/nyaf/fuzzer

# Clone the Rootfs repo provided by qiling. Here is going to be the root directory for the fuzzing target.
$ git clone https://github.com/qilingframework/rootfs

$ cd fuzzer

$ cat ./examples/linux/src/elite_bug.c
```

```c
#include <stdio.h>
#include <string.h>

char *serial = "\x31\x3e\x3d\x26\x31";

void win(){
  printf("You win!\n");

  // Let's crash something
  *(int*)0 = 0;
}

int check(char *ptr, int len)
{
  int i = 0;
  while (i < 5){
    if (((ptr[i] - 1) ^ 0x55) != serial[i])
      return 1;
    i++;
  }
  return 0;
}

int main(int argc, char **argv) {
  if(argc != 2) {
    printf("Bad args\n");
    return 1;
  }

  if(check(argv[1], strlen(argv[1])))
    printf("Bad pass: %s\n", argv[1]);
  else
    win();

  return 0;
}
```

```bash
# Lets check the configuration
$ cat ./examples/linux/arm64/elite_bug.yml
```


```yaml
cmd: ./examples/linux/arm64/elite_bug hola
rootfs: ../rootfs/arm64_linux
fuzzing:
  strategy: generational_search
  runners: 1
  symbolics: 1

workers: 3

seeds:
  - ./seed

timeout: 1000000000
start_address: 0x8b8
stop_address:
  - 0x934

injection:
  method: function
  address: 0x83c
```


```bash
# Create the seed
$ echo hola > ./seed 

# Run the fuzzer
$ ./docker_nyaf /home/tantrum/Dev/tesis/fuzzer/ ./examples/linux/arm64/elite_bug.yml

# And when it finish we could press `ctrl + c` to exit from the fuzzer, and we get:

#...
#[       Fuzzer       ] Bugs found:
#[       Fuzzer       ] {'bound': 5, 'coverage': {(2128, 2196), (2196, 2128), (2208, 2128), (1680, 2312), (2220, 2324), (2108, 2208), (2312, 2108), (2196, 2220), (2284, 1680), (2364, 2068), (1760, 2088), (2332, 2232), (2232, 2284), (2324, 2364), (2068, 1760)}, 'jump_count': 22, 'error': <Bug.EXCEPTION: 6>, 'time': 0, 'priority': 0, 'alternatives': [], 'data': b'elite'}
#...
```


## Configuration File ##
We use a YAML file to configure the fuzzing. Then in a single command we can start fuzzing our application.
A configuration file looks like this:

```yaml
# config.yml

cmd: ./tests/linux/arm64/elite_bug hola
rootfs: ../rootfs/arm64_linux
fuzzer: generational_search

workers: 3

seeds:
  - ./seed.json
  - ./seed2.json

timeout: 1000000000
start_address: 0x8b8
stop_address:
  - 0x838

injection:
  method: function
  address: 0x83c
```


### Attributes ###

#### Basic ####
```yaml
# Path to use as a file system root of the binary
rootfs: str

# Amount of processes to spown
workers: int

# Fuzzing strategy to use
fuzzer: str
```

#### Run setup ####
```yaml
# Command to run the program
cmd: str

# List of seeds (paths)
seeds: [str]

# Address where to restart the emulation every time (snapshot address)
start_address: int

# List of adress where to stop the emulation if is reached
stop_address: List[int]

# Timeout of a emulation run in ms
timeout: int

# Snapshot file where to start the emulation
snapshot: str
```


#### Injection ####
```yaml
# Injection related configuration
injection:
  
  # Method of payload injection selected
  method: str

  # Address where to inject the payload
  address: int
```
