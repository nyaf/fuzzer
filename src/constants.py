import sys
from enum import IntEnum

from lief import ARCHITECTURES as larchs
from lief import EXE_FORMATS as lformats


# Input interfaces
class INJECT(IntEnum):
    FUNC = 0


# CPU Arch
class ARCH(IntEnum):
    X86_64 = 102
    X86_32 = 101
    ARM32 = 103
    ARM64 = 105


class OS(IntEnum):
    LINUX = 0
    WINDOWS = 1
    UEFI = 2
    OSX = 3


# Possible bugs on run
class Bug(IntEnum):
    NONE = 0
    MEM_INVALID = 1
    MEM_UNMAPPED = 2
    READ_INVALID = 3
    WRITE_INVALID = 4
    FETCH_INVALID = 5
    EXCEPTION = 6
    UNDEFINED = 7
    UNDEF_EXCEPTION = 8
    TIMEOUT = 9
    DOUBLE_FREE = 10


class TRACE:
    NONE = "none"
    COVERAGE = "coverage"
    SYMBOLIC = "symbolic"


ABI_TABLE = {}

OS_STRING = {OS.LINUX: "Linux", OS.WINDOWS: "Windows", OS.UEFI: "UEFI"}

ARCH_STRING = {
    ARCH.ARM32: "ARM32",
    ARCH.ARM64: "ARM64",
    ARCH.X86_32: "x86",
    ARCH.X86_64: "x86_64",
}

lief_archs = {
    larchs.ARM: [ARCH.ARM32, None],
    larchs.ARM64: [None, ARCH.ARM64],
    larchs.X86: [ARCH.X86_32, ARCH.X86_64],
}

lief_os = {
    lformats.PE: OS.WINDOWS,
    lformats.ELF: OS.LINUX,
    lformats.MACHO: OS.OSX,
    lformats.UNKNOWN: OS.UEFI,
}

FUZZER_CHOICES = ["generational_search"]
INJECT_CHOICES = ["function"]

DEFAULT_INJECTOR = "function"

# Others
DEBUG = False
USAGE = f"Usage: \n\t{sys.argv[0]} [ config file ] [ output directory ]"

HEADER = """
                  ███╗   ██╗ ██╗   ██╗  █████╗  ███████╗
                  ████╗  ██║ ╚██╗ ██╔╝ ██╔══██╗ ██╔════╝
                  ██╔██╗ ██║  ╚████╔╝  ███████║ █████╗  
                  ██║╚██╗██║   ╚██╔╝   ██╔══██║ ██╔══╝  
                  ██║ ╚████║    ██║    ██║  ██║ ██║     
                  ╚═╝  ╚═══╝    ╚═╝    ╚═╝  ╚═╝ ╚═╝     
"""

FUTTER = """===============  Bye  ==============="""

DESC = "This is the fuzzer description"
EPILOG = "Bye..."
