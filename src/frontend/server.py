import os
import signal
import sys
import time
from io import TextIOWrapper
from math import ceil
from multiprocessing import Manager, Process, Value
from threading import Thread
from typing import Any, Callable, Dict, List, Optional

from rich.console import Console
from rich.layout import Layout
from rich.live import Live
from rich.panel import Panel
from rich.text import Text

from communication import IpConnection, IpcSockType
from constants import HEADER
from fuzzing.report import FuzzerReport


class LogsManager:
    def __init__(self) -> None:
        self._lines: List[str] = []

        self._console = Console()

    def _get_line_size(self, line: str) -> None:
        return ceil(len(line) / (self._console.size.width - 88))

    @property
    def _lines_number(self) -> int:
        return sum([self._get_line_size(line) for line in self._lines])

    @property
    def _console_size(self) -> int:
        size: int = self._console.size.height - 2
        return size if size > 0 else 0

    @property
    def _free_space(self) -> int:
        space = self._console_size - self._lines_number
        return space if space > 0 else 0

    def put_line(self, line: str) -> None:
        size: int = self._get_line_size(line)

        while size > self._free_space:
            self._lines.pop()
            self._lines = self._lines[1:]

        self._lines.append(line)

    @property
    def text(self) -> str:
        return "\n".join(self._lines)


class FuzzerTUI(Process):
    def __init__(self, report_ipc_id: int) -> None:
        super().__init__()

        self._layout: Layout
        self._report_ipc_id: int = report_ipc_id
        self._report_conn: IpConnection
        self._logs_conn: Optional[IpConnection] = None
        self._stop_flag: Optional[Value] = Manager().Value("i", 0)
        self._running: bool = False
        self._logs_file: Optional[TextIOWrapper] = None
        self._logs_manager: Optional[LogsManager] = None

    def _create_layout(self) -> None:
        self._layout = Layout()

        side = Layout(name="side")

        body = Layout(name="body", size=80)

        title = Layout(name="title", size=10)
        upper = Layout(name="upper")
        lower = Layout(name="lower")

        body.split_column(title, upper, lower)

        left = Layout(name="left")

        right = Layout(name="right")
        right_upper = Layout(name="upper")
        right_lower = Layout(name="lower")

        right.split_column(right_upper, right_lower)

        lower.split_row(left, right)

        self._layout.split_row(body, side)

    def _create_panels(self) -> None:
        fuzzer_panel = Panel(Text("Bugs found: "), title="Fuzzer")
        self._layout["body"]["upper"].update(fuzzer_panel)

        title_panel = Panel(Text(HEADER))
        self._layout["body"]["title"].update(title_panel)

    def _dict_to_panel(self, data: Dict[str, Any], title: str = "") -> Panel:
        text = Text(justify="center")

        for key, value in data.items():
            text.append(f"\n{key}: ", style="#cccccc")
            text.append(str(value))

        return Panel(text, title=title, border_style="#1d56b3")

    def _update_frame(self, report) -> None:
        self._layout["body"]["upper"].update(
            self._dict_to_panel(report.general_dict, title="General")
        )
        self._layout["body"]["lower"]["left"].update(
            self._dict_to_panel(report.fuzzer_info, title="Fuzzer")
        )
        self._layout["body"]["lower"]["right"]["upper"].update(
            self._dict_to_panel(report.pool_list[0].__dict__, title="Concolic runners")
        )
        self._layout["body"]["lower"]["right"]["lower"].update(
            self._dict_to_panel(report.pool_list[1].__dict__, title="Symbolic runners")
        )

    def _update_logs(self, line: str) -> None:
        if self._logs_file is not None:
            self._logs_file.write(line + "\n")
            self._logs_file.flush()

        self._logs_manager.put_line(line)
        self._layout["side"].update(
            Panel(
                Text(self._logs_manager.text, overflow="crop"),
                title="Logs",
                border_style="#1d56b3",
            )
        )

    def _pre_run(self) -> None:
        # Disable signal
        signal.signal(signal.SIGINT, signal.SIG_IGN)
        sys.stderr = open(os.devnull, "w")

        # Crete the reports connection
        self._report_conn = IpConnection(IpcSockType.PULL)
        self._report_conn.connect(self._report_ipc_id)

        self._logs_manager = LogsManager()

        # Create the logs connection
        logs_ipc: Optional[int] = os.getenv("LOG_GET_IPC_ID")
        if logs_ipc is not None:
            self._logs_conn = IpConnection(IpcSockType.PULL)
            self._logs_conn.connect(logs_ipc)

        # Open the file where to store the logs
        logs_file: Optional[str] = os.getenv("LOG_FILE_PATH")
        if logs_file is not None:
            self._logs_file = open(logs_file, "w")

    def _conn_poll(
        self, conn: IpConnection, callback: Callable, timeout: int = 1
    ) -> None:
        while self._running:
            try:
                element: Optional[FuzzerReport] = conn.recv(timeout=timeout)

                # Timeout reached
                if element is not None:
                    callback(element)

            except Exception:
                time.sleep(timeout)

    def run(self) -> None:
        # Init all the multiprocess communications
        self._pre_run()
        self._create_layout()
        self._create_panels()

        self._running = True

        try:
            with Live(self._layout):
                report_thread = Thread(
                    target=self._conn_poll,
                    args=(self._report_conn, self._update_frame),
                )

                report_thread.start()

                if self._logs_conn is not None:
                    logs_thread = Thread(
                        target=self._conn_poll,
                        args=(self._logs_conn, self._update_logs),
                    )

                    logs_thread.start()

                while self._stop_flag.value == 0:
                    time.sleep(1)

            self._running = False

            report_thread.join()
            if self._logs_conn is not None:
                logs_thread.join()

        except BrokenPipeError:
            pass

    def stop(self) -> None:
        if self._stop_flag is not None:
            self._stop_flag.set(1)
