from typing import Any, Dict, List

from emulation.runner import RunnerPoolReport


class FuzzerReport:
    def __init__(
        self,
        bugs: int,
        time: float,
        fuzzer_info: Dict[str, Any] = {},
        pool_list: List[RunnerPoolReport] = [],
    ) -> None:
        self.bugs: int = bugs
        self.time: float = time
        self.fuzzer_info: Dict[str, Any] = fuzzer_info
        self.pool_list: List[RunnerPoolReport] = pool_list

    @property
    def general_dict(self) -> Dict[str, Any]:
        return {"bugs": self.bugs, "time": self.time}
