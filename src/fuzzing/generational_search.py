import signal
from multiprocessing import Manager, Process
from queue import PriorityQueue
from typing import Optional

from communication import IpConnection, IpcSockType, IpQueue
from constants import Bug
from emulation.runner import RunnerPool
from fuzzing import FuzzerConfig
from utils.data import Payload
from utils.log import Logger, parse_payload_data

from .fuzzer import Fuzzer


class CoverageValuator(Process):
    """
    Process to valuate a report of a runner.

    Args:
        report_ipc (int): The IpConnection where receive reports.
        valuated_ipc (int): The IpConnection where send valuated reports.
        bug_ipc (int): The IpConnection where send bug reports.
    """

    def __init__(self, report_ipc: int, valuated_ipc: int, bug_ipc: int):
        super().__init__()

        self._logger: Optional[Logger] = None
        self._stop_flag = Manager().Value("i", 0)
        self.__coverage = set()
        self._report_ipc_id = report_ipc
        self._valuated_ipc_id = valuated_ipc
        self._bug_ipc_id = bug_ipc

    def _before_run(self) -> None:
        # Disable signal
        signal.signal(signal.SIGINT, signal.SIG_IGN)
        self._logger = Logger("CoverageValuator")

        # Init connections
        self.report_ipc = IpConnection(IpcSockType.PULL)
        self.report_ipc.connect(self._report_ipc_id)

        self.valuated_ipc = IpConnection(IpcSockType.PUSH)
        self.valuated_ipc.connect(self._valuated_ipc_id)

        self.bug_ipc = IpConnection(IpcSockType.PUSH)
        self.bug_ipc.connect(self._bug_ipc_id)

    def run(self) -> None:
        """Start the process."""
        self._before_run()

        try:
            while self._stop_flag.value == 0:
                report = self.report_ipc.recv(timeout=1000)

                # Timeout reached
                if report is None:
                    continue

                if report.error != Bug.NONE:
                    self.bug_ipc.send(report)

                self._valuate(report)

                self.valuated_ipc.send(report)
        except BrokenPipeError:
            pass

    def stop(self) -> None:
        """Stop the process."""
        self._stop_flag.value = 1

    def _valuate(self, job) -> None:
        """Put a priority to a report and colect the new coverage."""
        priority = len(job.coverage - self.__coverage)
        job.priority = priority
        self._logger.log(
            f"Valuator put priority of {priority} to ({parse_payload_data(job)})"
        )
        self.__coverage |= job.coverage


class GenerationalSearchFuzzer(Fuzzer):
    """
    Generational Search Fuzzer class.

    Read the docs for more info.
    """

    def __init__(self, config: FuzzerConfig, report_conn_id: int):
        super().__init__(config, report_conn_id)

        self.tc_queue = IpQueue(send_enable=True, recv_enable=True)
        self.report_queue = IpQueue(send_enable=True, recv_enable=True)
        # TODO: Priority Queue
        self.valuated_queue = IpQueue(
            send_enable=True, recv_enable=True, queue=PriorityQueue
        )

        self.runner_pool = RunnerPool(
            factory=self._runner_factory,
            trace="coverage",
            payload_ipc=self.tc_queue.get_conn_id,
            report_ipc=self.report_queue.put_conn_id,
            jobs=config.fuzzing.runners,
        )

        self.valuator = CoverageValuator(
            report_ipc=self.report_queue.get_conn_id,
            valuated_ipc=self.valuated_queue.put_conn_id,
            bug_ipc=self.bug_queue.put_conn_id,
        )

        self.concolic_pool = RunnerPool(
            factory=self._runner_factory,
            trace="concolic",
            payload_ipc=self.valuated_queue.get_conn_id,
            report_ipc=self.tc_queue.put_conn_id,
            jobs=config.fuzzing.symbolics,
        )

        # Put it on the test cases queue
        for seed in config.seeds:
            self.tc_queue.put(Payload(file_path=seed))

        self.start_status_routine()

    def status(self) -> dict:
        status = super().status()

        status.fuzzer_info = {
            "Report queue length": len(self.report_queue),
            "Report queue accum": self.report_queue.counter,
            "Valuated queue length": len(self.valuated_queue),
            "Valuated queue accum": self.valuated_queue.counter,
            "Testcase queue length": len(self.tc_queue),
            "Testcase queue accum": self.tc_queue.counter,
        }

        status.pool_list = [self.runner_pool.status(), self.concolic_pool.status()]

        return status

    def fuzz(self):
        """Init the fuzzer"""
        super().fuzz()
        self.runner_pool.start()
        self.valuator.start()
        self.concolic_pool.start()

    def stop(self):
        """Stop the fuzzer."""
        super().stop()

        self.runner_pool.stop()
        self.valuator.stop()
        self.concolic_pool.stop()
