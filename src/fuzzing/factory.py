import constants as const
from fuzzing.config import FuzzerConfig
from fuzzing.generational_search import GenerationalSearchFuzzer

FUZZER_TABLE = {"generational_search": GenerationalSearchFuzzer}


class FuzzerFactory:
    @staticmethod
    def new_fuzzer(config: FuzzerConfig, report_conn_id: int):
        if config.fuzzing.strategy not in FUZZER_TABLE:
            raise const.EXCEPT.BAD_ARG_FUZZER

        return FUZZER_TABLE[config.fuzzing.strategy](config, report_conn_id)
