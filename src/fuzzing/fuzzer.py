from abc import ABC, abstractmethod
from threading import Thread
from time import sleep, time
from typing import Optional

from communication import IpConnection, IpcSockType, IpQueue
from emulation.runner import RunnerFactory
from emulation.runner.config import RunnerConfig
from fuzzing.bugs import BugsManager
from fuzzing.config import FuzzerConfig
from fuzzing.report import FuzzerReport
from utils.log import Logger


class Fuzzer(ABC):
    """Abstract class for a fuzzer."""

    def __init__(self, config: FuzzerConfig, report_conn_id: int) -> None:
        self._logger: Optional[Logger] = None
        self._fuzz_time: float = 0.0
        self._report_connection: IpConnection

        # Connect to the report connection
        self._report_connection = IpConnection(IpcSockType.PUSH)
        self._report_connection.connect(report_conn_id)

        self.config: FuzzerConfig = config

        # Parse and set defaults args for the runners
        runner_config = RunnerConfig(self.config)

        # Create the runner factory
        self._runner_factory = RunnerFactory(runner_config)

        # Create bug queue
        self.bug_queue = IpQueue(send_enable=True, recv_enable=True)

        # Create bugs manager
        self.bugs_manager = BugsManager(self.bug_queue.get_conn_id, config.out_dir)

    def status(self) -> FuzzerReport:
        report = FuzzerReport(bugs=len(self.bug_queue), time=(time() - self._fuzz_time))

        return report

    def start_status_routine(self) -> None:
        def status_loop() -> None:
            while True:
                sleep(0.5)
                self._report_connection.send(self.status())

        Thread(target=status_loop, daemon=True).start()

    def stop(self) -> None:
        """Stop the fuzzer execution."""
        self._logger.log("Stopping the fuzzer")

        if self.bug_queue.empty():
            self._logger.success("No bugs found")
        else:
            self._logger.success("Bugs found:")

            while len(self.bug_queue) != 0:
                self._logger.error(self.bug_queue.get().__dict__)

        self.bugs_manager.stop()

        self._logger.error("Fuzzer stopped")

    @abstractmethod
    def fuzz(self) -> None:
        """Start fuzzing"""
        self._logger = Logger("Fuzzer")
        self.bugs_manager.start()

        self._fuzz_time = time()
