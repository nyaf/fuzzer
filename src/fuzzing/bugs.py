import os
import signal
from multiprocessing import Manager, Process, Value
from typing import Optional

from communication import IpConnection, IpcSockType
from utils.data import Payload


class BugsManager(Process):
    def __init__(self, bugs_ipc: int, dir: str) -> None:
        super().__init__()

        self._stop_flag: Optional[Value] = Manager().Value("i", 0)

        self._bugs_ipc: int = bugs_ipc
        self._bugs_dir: str = dir

    def _create_connection(self) -> IpConnection:
        conn = IpConnection(IpcSockType.PULL)
        conn.connect(self._bugs_ipc)

        return conn

    def _bug_to_file(self, bug: Payload) -> None:
        data_hash: str = str(abs(hash(bug.data)))

        with open(os.path.join(self._bugs_dir, data_hash), "wb") as bug_file:
            bug_file.write(bug.data)

    def run(self) -> None:
        # Disable signal
        signal.signal(signal.SIGINT, signal.SIG_IGN)

        bugs_conn = self._create_connection()

        try:
            while self._stop_flag.value == 0:
                bug = bugs_conn.recv(timeout=1000)

                # Timeout reached
                if type(bug) is not Payload:
                    continue

                self._bug_to_file(bug)

        except BrokenPipeError:
            pass

    def stop(self) -> None:
        if self._stop_flag is not None:
            self._stop_flag.set(1)
