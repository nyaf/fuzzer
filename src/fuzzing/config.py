from os import mkdir
from os.path import exists, isdir, isfile
from pathlib import Path
from typing import Any, Dict, List, Optional

from yaml import SafeLoader, load

from emulation.runner.injector.config import (FunctionInjectionConfig,
                                              InjectionConfig)
from utils.config import Config


class FuzzingConfig(Config):
    def __init__(self, config: Dict[str, Any]) -> None:
        self.strategy: str

        self.strategy = self._parse_config(
            "strategy",
            config,
            arg_type=str,
            default="generational_search",
            required=True,
        )


class GenerationalSearchConfig(FuzzingConfig):
    def __init__(self, config: dict) -> None:
        super().__init__(config)

        self.runners: int = self._parse_config("runners", config, arg_type=int)
        self.symbolics: int = self._parse_config("symbolics", config, arg_type=int)


class FuzzerConfig(Config):
    """
    Class used to store the fuzzing configs.

    Attributes:
        cmd (List[str]): Command to run the target program.
        rootfs (str): Path to the fake root for the target program.
        fuzzer (str): Selected fuzzer type.
        snapshot (Optional[str]): snapshot to restore on start.
        wokers (Optional[int]): Number of runners (processes) to use (All types included).
        seeds (List[str]): Path to the seeds to use as first input.
        timeout (Optional[int]): Timeout in ms of a run.
        start_address (Optinoal[int]): Address where to start the emulation.
        stop_address (Optional[int]): Address where to stop the emulation.
        injection (Dict[str, Any]): Injection configuration.
    """

    def __init__(self, config_file: str, out_dir: str) -> None:
        """
        Args:
            config_path (str): Path to the configuration YAML file.
        """
        self.cmd: List[str]
        self.rootfs: str
        self.fuzzer: str
        self.snapshot: Optional[str]
        self.wokers: Optional[int]
        self.seeds: List[str]
        self.timeout: Optional[int]
        self.start_address: Optional[int]
        self.stop_address: List[int]
        self.injection: InjectionConfig
        self.fuzzing: Config

        self.out_dir: str = self._check_out_dir(out_dir)
        self._config_from_file(config_file)

    @staticmethod
    def _check_out_dir(dir: str) -> str:
        path = Path(dir)
        if not isdir(path.parent):
            raise ValueError("Invalid output directory")

        if isdir(path):
            pass
        # raise ValueError("Output directory already exists")

        else:
            mkdir(path)

        return dir

    def _config_from_file(self, config_path: str) -> None:
        with open(config_path, "r") as config_file:
            config_dict = load(config_file, SafeLoader)

            self.cmd = self._parse_config("cmd", config_dict, arg_type=str).split(" ")
            self.rootfs = self._parse_config("rootfs", config_dict, arg_type=str)
            self.seeds = self._parse_config("seeds", config_dict, arg_type=list)
            self.snapshot = self._parse_config(
                "snapshot", config_dict, arg_type=str, default=None, required=False
            )
            self.timeout = self._parse_config(
                "timeout", config_dict, arg_type=int, default=None, required=False
            )
            self.start_address = self._parse_config(
                "start_address", config_dict, arg_type=int, default=None, required=False
            )
            self.stop_address = self._parse_config(
                "stop_address", config_dict, arg_type=list, default=[], required=False
            )

            self.injection = self._parse_injection_config(config_dict["injection"])

            self.fuzzing = self._parse_fuzzing_config(config_dict)

            self._checks()

    def _parse_injection_config(self, config: Dict[str, Any]) -> InjectionConfig:
        method: str = self._parse_config("method", config, arg_type=str)

        if method == "function":
            return FunctionInjectionConfig(config)

        else:
            raise ValueError("Injection method not found")

    def _parse_fuzzing_config(self, config: Dict) -> FuzzingConfig:
        fuzzing_dict = self._parse_config("fuzzing", config, arg_type=dict)
        config = FuzzingConfig(fuzzing_dict)

        if config.strategy == "generational_search":
            return GenerationalSearchConfig(fuzzing_dict)

        else:
            raise ValueError("Fuzzing strategy not found")

    def _checks(self) -> None:
        if not exists(self.rootfs) or isfile(self.rootfs):
            raise FileNotFoundError(self.rootfs)

        if self.timeout is not None and self.timeout < 0:
            raise ValueError("Timeout has to be a positive number")

        for seed in self.seeds:
            if not exists(seed) or not isfile(seed):
                raise ValueError(f"Seed not found: {seed}")
