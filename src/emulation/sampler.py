import time

from emulation.engine import Engine
from utils.exceptions import RunningException, UnreachedHook
from utils.log import Logger


class Sampler(Engine):
    def __init__(
        self, path, rootfs, stop_address=None, timeout=None, count=1, filename=None
    ):
        super().__init__(path, rootfs)

        self.timeout = timeout
        self.count = count
        self.filename = filename
        self.snapshot = None
        self.__set_hook_addr(stop_address)
        self._ql.hook_address(self.__addr_hook, self.hook_addr)
        self.__logger = Logger("Sampler")

    def run(self):
        self.__logger.info("Sampling start")

        try:
            super().run()
        except Exception as e:
            raise RunningException(e)

        if self.snapshot is None:
            raise UnreachedHook

    def __set_hook_addr(self, stop_addr):
        if stop_addr is not None:
            self.hook_addr = self.base_addr + stop_addr
        else:
            self.hook_addr = self.base_addr + self.entry_point

    def __addr_hook(self, ql):
        self.count -= 1
        if self.count == 0:
            self.__make_snapshot(ql)

    def __make_snapshot(self, ql):
        if self.filename is None:
            self.filename = f"/tmp/snapshot-{int(time.time())}"

        ql.save(cpu_context=True, snapshot=self.filename)
        self.snapshot = self.filename

        self.stop()

        self.__logger.success("Snapshot taken")
