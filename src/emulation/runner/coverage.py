from typing import Optional

from emulation.runner.base import BaseRunner
from emulation.runner.config import RunnerConfig
from utils.data import Payload


class CoverageRunner(BaseRunner):
    def __init__(self, config: RunnerConfig, iden: Optional[int] = None):
        super().__init__(config, iden)

        self._ql.hook_block(self.__block_hook)
        self.__last_block = self.start_address
        self.coverage = set()
        self.jumps = 0

    def run(self, payload: Payload):
        self.coverage = set()
        self.jumps = 0

        super().run(payload)
        payload.coverage = self.coverage
        payload.jump_count = self.jumps

    def __block_hook(self, _, address, __):
        if self.end_addr > address > self.base_addr:
            self.jumps += 1
            self.coverage.add(
                (self.__last_block - self.base_addr, address - self.base_addr)
            )
            self.__last_block = address
