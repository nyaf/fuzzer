from abc import ABC, abstractmethod


class Runner(ABC):
    """ """

    @abstractmethod
    def start(self) -> None:
        """ """

    @abstractmethod
    def stop(self) -> None:
        """ """

    @abstractmethod
    def clean(self) -> None:
        """ """
