from copy import copy
from typing import List, Optional

import lief

from constants import ARCH, lief_archs
from emulation import Sampler
from emulation.runner.injector.config import InjectionConfig


class RunnerConfig:
    def __init__(
        self,
        fuzzer_config,
    ) -> None:
        self.cmd: List[str]
        self.rootfs: List[str]
        self.arch: ARCH
        self.injection: InjectionConfig
        self.snapshot: str
        self.start_address: Optional[int]
        self.stop_address: List[int]
        self.timeout: Optional[int] = None

        self.cmd = fuzzer_config.cmd.copy()
        self.rootfs = fuzzer_config.rootfs
        self.injection = copy(fuzzer_config.injection)

        self.start_address = fuzzer_config.start_address
        self.stop_address = fuzzer_config.stop_address

        self.arch = self._get_arch(self.cmd[0])

        self.snapshot = (
            fuzzer_config.snapshot if fuzzer_config.snapshot else self._take_snapshot()
        )
        # self.timeout = timeout if timeout else self._estimate_timeout()

    def _estimate_timeout(self, seeds: List[str]) -> int:
        """
        Calculate the timeout with the seeds.
        This will run a timer with all the seeds to calculate the time.
        # TODO: Timeout in structions?

        Args:
            args (dict): Arguments to the runner.
            return (int): Estimated time to timeout the runners.
        """
        # TODO
        # timer = RunnerFactory(self).create(trace="timer")

        # Get the average time of run all seeds
        # total_time: int = 0
        # for seed in self._s:
        #     payload = Payload(file_path=seed)
        #     timer.run(payload)

        #     total_time += payload.time

        # avg_time = total_time / len(self._seeds)

        # Convert, add more time and return
        # return int(avg_time * 1000000)

    def _take_snapshot(self) -> str:
        """
        Take the snapshot and save it to a file.

        Args:
            args (dict): Arguments to the runner.
            return (str): Filename of the snapshot.
        """
        sampler = Sampler(self.cmd, self.rootfs, self.start_address)
        sampler.run()
        return sampler.snapshot

    @staticmethod
    def _get_arch(binary_path: str) -> ARCH:
        """
        Get the architecture of the binary.

        Args:
            binary_path (str): Path to the binary.
            return (str): The architecture.
        """
        binary = lief.parse(binary_path).abstract

        arch = binary.header.architecture
        bites = binary.header.is_64

        return lief_archs[arch][bites]
