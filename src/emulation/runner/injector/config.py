from typing import Any, Dict, Optional

from utils.config import Config


class InjectionConfig(Config):
    """
    Class used to store the injection configs.

    Attributes:
        method (str): How to inject the payload.
        address (Optional[int]): Address where to inject. The start_address if not give.
    """

    def __init__(self, config: Dict[str, Any]) -> None:
        """
        Args:
            config (Dict[str, Any]): Dictionary with the configuration arguments.
        """
        self.method: str
        self.address: Optional[int]

        self.method = self._parse_config("method", config, arg_type=str)
        self.address = self._parse_config(
            "address", config, arg_type=int, default=None, required=False
        )


class FunctionInjectionConfig(InjectionConfig):
    def __init__(self, config: Dict[str, Any]) -> None:
        super().__init__(config)

        self.ptr_pos: int
        self.len_pos: int
        self.max_len: Optional[int]
        self.end: Optional[bytes]

        self.ptr_pos = self._parse_config(
            "ptr_pos", config, arg_type=int, default=0, required=False
        )

        self.len_pos = self._parse_config(
            "len_pos", config, arg_type=int, default=1, required=False
        )

        self.max_len = self._parse_config(
            "max_len", config, arg_type=int, default=None, required=False
        )

        end_str = self._parse_config(
            "end", config, arg_type=str, default=None, required=False
        )

        if end_str is not None:
            self.end = end_str.encode().decode("unicode-escape")
        else:
            self.end = None

        if self.ptr_pos == self.len_pos:
            self.len_pos += 1
