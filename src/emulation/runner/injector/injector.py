from abc import ABC, abstractmethod

from emulation.runner.injector.config import InjectionConfig
from utils.data import Payload


class AbstractInjector(ABC):
    def __init__(self, config: InjectionConfig) -> None:
        self._address = config.address

    def setup(self, runner) -> None:
        self._runner = runner
        self._address += runner.base_addr
        runner._ql.hook_address(self._inject_hook, self._address)

    @abstractmethod
    def inject(self, runner, payload: Payload) -> None:
        pass

    @staticmethod
    @abstractmethod
    def _inject_hook(runner, payload: Payload) -> None:
        pass
