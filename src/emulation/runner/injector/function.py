from typing import Optional

from emulation.runner.injector.config import InjectionConfig
from emulation.runner.injector.injector import AbstractInjector
from utils.data import Payload


class FunctionInjector(AbstractInjector):
    def __init__(self, config: InjectionConfig):
        super().__init__(config)

        self.ptr_pos: int = config.ptr_pos
        self.len_pos: int = config.len_pos
        self.max_len: int = config.max_len
        self.end: bytes = config.end

        self._runner = None
        self._payload: Optional[bytes] = None
        self._config = config

    def inject(self, runner, payload: Payload):
        self._payload = payload
        self._runner = runner

    def _inject_hook(self, ql):
        address = self._runner._alloc_mem(len(self._payload))
        ql.mem.write(address, self._payload.data)

        if self.max_len is not None:
            self._payload.data = self._payload.data[: self.max_len - 1]

        if self.end is not None:
            self._payload.data += bytes(self.end, "utf-8")

        self._runner._abi.set_arg(self._runner._ql, self.ptr_pos, address)
        if self.len_pos >= 0:
            self._runner._abi.set_arg(
                self._runner._ql, self.len_pos, len(self._payload)
            )

        if self._runner._after_inject is not None:
            self._runner._after_inject(self._payload)
