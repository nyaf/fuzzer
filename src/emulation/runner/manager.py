import os
import signal
import sys
import time
from dataclasses import dataclass
from multiprocessing import Manager, Process
from threading import Thread

import constants as const
from communication import IpConnection, IpcSockType
from emulation.runner import RunnerFactory
from utils.log import Logger, parse_payload_data


class RunnerProcess(Process):
    """
    Process to run a Runner in a separate process.
    The process will get reports from 'payload_ipc' and send the report
    to 'report_ipc'.

    Args:
        factory (RunnerFactory): Factory to create the runner.
        trace (str): The trace method of the runner (coverage, concolic,...).
        payload_ipc (int): Id of the IpConnection where pull for payloads.
        report_ipc (int): Id of the IpConnection where push the reports.
        iden (int): Process identifier (for debug only).
    """

    def __init__(
        self,
        factory: RunnerFactory,
        trace: str,
        payload_ipc: int,
        report_ipc: int,
        iden: int,
    ):
        super().__init__()
        self.iden = iden
        self._factory = factory
        self._trace = trace
        self._payload_ipc_id = payload_ipc
        self._report_ipc_id = report_ipc
        self._exit_flag = Manager().Value("i", 0)

    def _bug_found(self, payload) -> None:
        self._logger.error(
            f"BUG FOUND ({const.Bug(payload.error).name}): {hex(payload.address)}"
        )

    def _send_report(self, payload) -> None:
        if self._trace == "concolic":
            for alt in payload.alternatives:
                self._report_ipc.send(alt)

        else:
            self._report_ipc.send(payload)

    def run(self) -> None:
        """Start the process."""
        # Disable signal
        signal.signal(signal.SIGINT, signal.SIG_IGN)

        sys.stderr = open(os.devnull, "w")

        self._logger = Logger(f"Runner {self.iden} ({self._trace})")
        # Create a runner
        self._runner = self._factory.create(self._trace)

        # Init the connections
        self._payload_ipc = IpConnection(IpcSockType.PULL)
        self._payload_ipc.connect(self._payload_ipc_id)

        self._report_ipc = IpConnection(IpcSockType.PUSH)
        self._report_ipc.connect(self._report_ipc_id)

        while self._exit_flag.value == 0:
            payload = self._payload_ipc.recv(timeout=1000)

            # Timeout reached
            if payload is None:
                continue

            self._logger.log(f"Running: {parse_payload_data(payload)}")
            self._runner.run(payload)

            if payload.error != const.Bug.NONE:
                self._bug_found(payload)

            self._send_report(payload)

        self._runner.clean()

    def stop(self):
        """Stop and exit."""
        self._exit_flag.set(1)


@dataclass
class RunnerPoolReport:
    target_runners: int
    actual_runners: int
    errors: int
    status: str


class RunnerPool:
    """
    Pool of processes with runners.

    Args:
        factory (RunnerFactory): Factory to create the runners.
        trace (str): The trace method of the runners (coverage, concolic,...).
        payload_ipc (int): Id of the IpConnection where pull for payloads.
        report_ipc (int): Id of the IpConnection where push the reports.
        jobs (int): Number of runners to init.
    """

    def __init__(
        self,
        factory: RunnerFactory,
        trace: str,
        payload_ipc: int,
        report_ipc: int,
        jobs=1,
    ) -> None:
        self._runners = []
        self._jobs: int = jobs
        self._errors: int = 0
        self._stop = False

        self._runner_args = [
            factory,
            trace,
            payload_ipc,
            report_ipc,
        ]

        for i in range(jobs):
            self._runners.append(RunnerProcess(*self._runner_args + [i]))

        Thread(target=self._runners_checker, daemon=True).start()

        self._logger = Logger("Manager")

        self._logger.success(f"New manager created with {jobs} Runners ({trace})")

    def start(self) -> None:
        """Start the runner processes."""
        for proc in self._runners:
            proc.start()

    def _runners_checker(self) -> None:
        while not self._stop:
            for runner in self._runners:
                if runner.exitcode is not None:
                    self._runners.remove(runner)
                    new_runner = RunnerProcess(*self._runner_args + [self._jobs])
                    new_runner.start()
                    self._runners.append(new_runner)
                    self._jobs += 1
                    self._errors += 1

                time.sleep(1)

    def stop(self) -> None:
        """Stop the runners and wait to exit."""
        self._stop = True

        for proc in self._runners:
            proc.stop()

        for proc in self._runners:
            proc.join()

        self._exit()

    def kill(self) -> None:
        """Kill all the runners."""
        for proc in self._runners:
            proc.kill()

        self._exit()

    def status(self) -> dict:
        """Return the status of the runner processes."""
        return RunnerPoolReport(
            self._jobs,
            len(self._runners),
            self._errors,
            "Stopped" if self._stop else "Running",
        )
        return {"runners": len(self._runners), "errors": self._errors}

    def _exit(self) -> None:
        self._logger.error("Manager stopped")
