import triton
from qiling import Qiling
from triton import TritonContext

from emulation.runner.abi.abi import AbstractAbi


class ARM64Abi(AbstractAbi):
    def __init__(self):
        self.reg_list = ["X0", "X1", "X2", "X3", "X4", "X5", "X6", "X7"]

    def setup(self, runner):
        pass

    @staticmethod
    def set_tt_arch(tt: TritonContext):
        tt.setArchitecture(triton.ARCH.AARCH64)

    def set_arg(self, ql: Qiling, position: int, payload: [bytes]):
        ql.arch.regs.write(self.reg_list[position], payload)

    @staticmethod
    def get_regs_table(ql: Qiling, tt: TritonContext) -> dict:
        table = {
            tt.registers.x0: ql.arch.regs.x0,
            tt.registers.x1: ql.arch.regs.x1,
            tt.registers.x2: ql.arch.regs.x2,
            tt.registers.x3: ql.arch.regs.x3,
            tt.registers.x4: ql.arch.regs.x4,
            tt.registers.x5: ql.arch.regs.x5,
            tt.registers.x6: ql.arch.regs.x6,
            tt.registers.x7: ql.arch.regs.x7,
            tt.registers.x8: ql.arch.regs.x8,
            tt.registers.x9: ql.arch.regs.x9,
            tt.registers.x10: ql.arch.regs.x10,
            tt.registers.x11: ql.arch.regs.x11,
            tt.registers.x12: ql.arch.regs.x12,
            tt.registers.x13: ql.arch.regs.x13,
            tt.registers.x14: ql.arch.regs.x14,
            tt.registers.x15: ql.arch.regs.x15,
            tt.registers.x16: ql.arch.regs.x16,
            tt.registers.x17: ql.arch.regs.x17,
            tt.registers.x18: ql.arch.regs.x18,
            tt.registers.x19: ql.arch.regs.x19,
            tt.registers.x20: ql.arch.regs.x20,
            tt.registers.x21: ql.arch.regs.x21,
            tt.registers.x22: ql.arch.regs.x22,
            tt.registers.x23: ql.arch.regs.x23,
            tt.registers.x24: ql.arch.regs.x24,
            tt.registers.x25: ql.arch.regs.x25,
            tt.registers.x26: ql.arch.regs.x26,
            tt.registers.x27: ql.arch.regs.x27,
            tt.registers.x28: ql.arch.regs.x28,
            tt.registers.x29: ql.arch.regs.x29,
            tt.registers.x30: ql.arch.regs.x30,
            tt.registers.w0: ql.arch.regs.x0,
            tt.registers.w1: ql.arch.regs.x1,
            tt.registers.w2: ql.arch.regs.x2,
            tt.registers.w3: ql.arch.regs.x3,
            tt.registers.w4: ql.arch.regs.x4,
            tt.registers.w5: ql.arch.regs.x5,
            tt.registers.w6: ql.arch.regs.x6,
            tt.registers.w7: ql.arch.regs.x7,
            tt.registers.w8: ql.arch.regs.x8,
            tt.registers.w9: ql.arch.regs.x9,
            tt.registers.w10: ql.arch.regs.x10,
            tt.registers.w11: ql.arch.regs.x11,
            tt.registers.w12: ql.arch.regs.x12,
            tt.registers.w13: ql.arch.regs.x13,
            tt.registers.w14: ql.arch.regs.x14,
            tt.registers.w15: ql.arch.regs.x15,
            tt.registers.w16: ql.arch.regs.x16,
            tt.registers.w17: ql.arch.regs.x17,
            tt.registers.w18: ql.arch.regs.x18,
            tt.registers.w19: ql.arch.regs.x19,
            tt.registers.w20: ql.arch.regs.x20,
            tt.registers.w21: ql.arch.regs.x21,
            tt.registers.w22: ql.arch.regs.x22,
            tt.registers.w23: ql.arch.regs.x23,
            tt.registers.w24: ql.arch.regs.x24,
            tt.registers.w25: ql.arch.regs.x25,
            tt.registers.w26: ql.arch.regs.x26,
            tt.registers.w27: ql.arch.regs.x27,
            tt.registers.w28: ql.arch.regs.x28,
            tt.registers.w29: ql.arch.regs.x29,
            tt.registers.w30: ql.arch.regs.x30,
            tt.registers.sp: ql.arch.regs.sp,
            tt.registers.pc: ql.arch.regs.pc,
            tt.registers.xzr: 0,
            tt.registers.wzr: 0,
        }

        return table
