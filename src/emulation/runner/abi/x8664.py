import triton
from qiling import Qiling
from triton import TritonContext

from emulation.runner.abi.abi import AbstractAbi


class X8664Abi(AbstractAbi):
    def __init__(self):
        self.reg_list = [
            "RDI",
            "RSI",
            "RDX",
            "R10",
            "R9",
            "R8",
            "R7",
            "R6",
            "R5",
            "R4",
            "R3",
            "R2",
            "R1",
            "R0",
        ]

    def setup(self, runner):
        pass

    @staticmethod
    def set_tt_arch(tt: TritonContext):
        tt.setArchitecture(triton.ARCH.X86_64)

    def set_arg(self, ql: Qiling, position: int, payload: [bytes]):
        ql.arch.regs.write(self.reg_list[position], payload)

    @staticmethod
    def get_regs_table(ql: Qiling, tt: TritonContext) -> dict:
        table = {
            tt.registers.rax: ql.arch.regs.rax,
            tt.registers.rbx: ql.arch.regs.rbx,
            tt.registers.rcx: ql.arch.regs.rcx,
            tt.registers.rdx: ql.arch.regs.rdx,
            tt.registers.rdi: ql.arch.regs.rdi,
            tt.registers.rsi: ql.arch.regs.rsi,
            tt.registers.r8: ql.arch.regs.r8,
            tt.registers.r9: ql.arch.regs.r9,
            tt.registers.r10: ql.arch.regs.r10,
            tt.registers.r11: ql.arch.regs.r11,
            tt.registers.r12: ql.arch.regs.r12,
            tt.registers.r13: ql.arch.regs.r13,
            tt.registers.r14: ql.arch.regs.r14,
            tt.registers.r15: ql.arch.regs.r15,
            tt.registers.rbp: ql.arch.regs.rbp,
            tt.registers.rsp: ql.arch.regs.rsp,
            tt.registers.rip: ql.arch.regs.rip,
            tt.registers.eflags: ql.arch.regs.eflags,
            tt.registers.fs: ql.arch.regs.fs,
            tt.registers.gs: ql.arch.regs.gs,
        }

        return table
