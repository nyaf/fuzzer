from abc import ABC, abstractmethod

from qiling import Qiling
from triton import TritonContext


class AbstractAbi(ABC):
    @abstractmethod
    def setup(self, runner):
        pass

    @staticmethod
    @abstractmethod
    def set_arg(ql: Qiling, position: int, payload: [bytes]):
        pass

    @staticmethod
    @abstractmethod
    def get_regs_table(ql: Qiling, tt: TritonContext) -> dict:
        pass

    @staticmethod
    @abstractmethod
    def set_tt_arch(tt: TritonContext):
        pass
