import triton
from qiling import Qiling
from triton import TritonContext

from emulation.runner.abi.abi import AbstractAbi


class X86Abi(AbstractAbi):
    @staticmethod
    def set_tt_arch(tt: TritonContext):
        tt.setArchitecture(triton.ARCH.X86)

    def setup(self, runner):
        pass

    @staticmethod
    def set_arg(ql: Qiling, position: int, payload: [bytes]):
        stack_offset = 4 * (position + 1)
        ql.mem.write(ql.arch.regs.esp + stack_offset, ql.pack32(payload))

    @staticmethod
    def get_regs_table(ql: Qiling, tt: TritonContext) -> dict:
        table = {
            tt.registers.eax: ql.arch.regs.eax,
            tt.registers.ebx: ql.arch.regs.ebx,
            tt.registers.ecx: ql.arch.regs.ecx,
            tt.registers.edx: ql.arch.regs.edx,
            tt.registers.edi: ql.arch.regs.edi,
            tt.registers.esi: ql.arch.regs.esi,
            tt.registers.ebp: ql.arch.regs.ebp,
            tt.registers.esp: ql.arch.regs.esp,
            tt.registers.eip: ql.arch.regs.eip,
            tt.registers.eflags: ql.arch.regs.eflags,
            tt.registers.fs: ql.arch.regs.fs,
            tt.registers.gs: ql.arch.regs.gs,
        }

        return table
