import triton
from qiling import Qiling
from triton import TritonContext

from emulation.runner.abi.abi import AbstractAbi


class ARM32Abi(AbstractAbi):
    def __init__(self):
        self.reg_list = ["R0", "R1", "R2", "R3"]

    def setup(self, runner):
        pass

    @staticmethod
    def set_tt_arch(tt: TritonContext):
        tt.setArchitecture(triton.ARCH.ARM32)

    def set_arg(self, ql: Qiling, position: int, payload: [bytes]):
        ql.arch.regs.write(self.reg_list[position], payload)

    @staticmethod
    def get_regs_table(ql: Qiling, tt: TritonContext) -> dict:
        table = {
            tt.registers.r0: ql.arch.regs.r0,
            tt.registers.r1: ql.arch.regs.r1,
            tt.registers.r2: ql.arch.regs.r2,
            tt.registers.r3: ql.arch.regs.r3,
            tt.registers.r4: ql.arch.regs.r4,
            tt.registers.r5: ql.arch.regs.r5,
            tt.registers.r6: ql.arch.regs.r6,
            tt.registers.r7: ql.arch.regs.r7,
            tt.registers.r8: ql.arch.regs.r8,
            tt.registers.r9: ql.arch.regs.r9,
            tt.registers.r10: ql.arch.regs.r10,
            tt.registers.r11: ql.arch.regs.r11,
            tt.registers.r12: ql.arch.regs.r12,
            tt.registers.sp: ql.arch.regs.sp,
            tt.registers.r14: ql.arch.regs.lr,
            tt.registers.pc: ql.arch.regs.pc,
        }

        return table
