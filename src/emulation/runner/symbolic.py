import time

import triton

from emulation.runner.base import BaseRunner
from emulation.runner.config import RunnerConfig
from utils.data import Payload


class SymbolicRunner(BaseRunner):
    def __init__(self, config: RunnerConfig, iden=None):
        super().__init__(config, iden)

        self.__debug_time = time.time()

        self._alternatives: list[Payload] = []
        self._ql.hook_code(self.__instruction_hook)
        self._after_inject = self.__inject_sym
        self.libc_enable = False
        self.__overflow_detect = False
        self.__mem_hook_lock = False

    def _init_symbolic(self) -> None:
        self._alternatives: list[Payload] = []

        self._tt = triton.TritonContext()
        self._abi.set_tt_arch(self._tt)

        # Try to change the solver, use Z3 if not exists
        try:
            self._tt.setSolver(triton.SOLVER.BITWUZLA)
        except AttributeError:
            pass

        self._tt.setAstRepresentationMode(triton.AST_REPRESENTATION.PYTHON)
        self._tt.addCallback(
            triton.CALLBACK.GET_CONCRETE_MEMORY_VALUE, self.__concrete_mem_hook
        )
        self._tt.addCallback(
            triton.CALLBACK.GET_CONCRETE_REGISTER_VALUE, self.__concrete_reg_hook
        )

        self._tt.setMode(triton.MODE.ALIGNED_MEMORY, True)
        self._tt.setMode(triton.MODE.ONLY_ON_SYMBOLIZED, True)
        self._tt.setMode(triton.MODE.AST_OPTIMIZATIONS, True)
        self._tt.setMode(triton.MODE.CONCRETIZE_UNDEFINED_REGISTERS, True)
        self._tt.setMode(triton.MODE.CONSTANT_FOLDING, True)
        self._tt.setMode(triton.MODE.PC_TRACKING_SYMBOLIC, True)

        self.__overflow_detect = False
        self.__mem_hook_lock = False

        self.__sym_vars = []

    def __get_changes_for_path(self, constraint):
        # TODO: This has to be outside symbolic runner
        model = self._tt.getModel(constraint)
        changes = {}

        for key, value in list(model.items()):
            sym_var = self._tt.getSymbolicVariable(key)
            # Save the new input as seed.
            byte_addr_accessed = sym_var.getOrigin()
            changes.update({byte_addr_accessed - self._alloc_addr: value.getValue()})

        return changes

    def __enqueue_payload(self, payload):
        self._alternatives.append(payload)

    def __get_new_payloads(self, payload: Payload):
        pcs = self._tt.getPathConstraints()
        ast_ctxt = self._tt.getAstContext()
        current_path_constraint = ast_ctxt.equal(ast_ctxt.bvtrue(), ast_ctxt.bvtrue())

        if self.__overflow_detect:
            self.__overflow_detect = False

        for pc_index in range(payload.bound, len(pcs)):
            path_constraint = pcs[pc_index]
            branches = path_constraint.getBranchConstraints()
            # If there is a condition on this path (not a direct jump), try to reverse it
            if path_constraint.isMultipleBranches():
                taken_address = path_constraint.getTakenAddress()
                for branch in branches:
                    # Get the constraint of the branch which has been not taken
                    if branch["dstAddr"] != taken_address:
                        # Check if we can change current executed path with the branch changed
                        desired_constrain = ast_ctxt.land(
                            [current_path_constraint, branch["constraint"]]
                        )
                        changes = self.__get_changes_for_path(desired_constrain)

                        if changes:
                            new_data = bytearray(payload.data)
                            for index in changes:
                                new_data += b"A" * max(index - len(new_data) + 1, 0)
                                new_data[index] = changes[index] % 256
                            self.__enqueue_payload(
                                Payload(data=bytes(new_data), bound=pc_index + 1)
                            )

    def __print_time(self):
        new_time = time.time()
        self._logger.info(f"TIME: {new_time - self.__debug_time}")
        self.__debug_time = new_time

    def run(self, payload: Payload):
        self._init_symbolic()

        #  self.__print_time()
        super().run(payload)
        #  self.__print_time()
        self.__get_new_payloads(payload)
        #  self.__print_time()
        payload.alternatives = self._alternatives

    def __new_sym_var(self, index):
        self.__mem_hook_lock = True
        sym_var = self._tt.symbolizeMemory(
            triton.MemoryAccess(self._alloc_addr + index, 8)
        )
        self.__mem_hook_lock = False
        self.__sym_vars.append(sym_var)

    def __inject_sym(self, payload):
        var_len = len(self.__sym_vars)
        len_diff = len(payload) - var_len

        if len_diff > 0:
            for i in range(var_len, len(payload)):
                self.__new_sym_var(i)

        for i in range(len(payload)):
            self._tt.setConcreteVariableValue(self.__sym_vars[i], payload.data[i])

    def __instruction_hook(self, _, addr, size):
        # if self.libc_enable or (self.base_addr <= addr <= self.end_addr):

        try:
            tt_inst = triton.Instruction(bytes(self._ql.mem.read(addr, size)))
            tt_inst.setAddress(addr)

            # Process the Triton instruction
            self._tt.processing(tt_inst)

        except Exception as exception:
            self._logger.error(
                f"Error processing an instruction {(hex(addr), size)} with Triton: {exception}"
            )

    def __concrete_reg_hook(self, tt, reg) -> None:
        try:
            reg_table = self._abi.get_regs_table(self._ql, self._tt)
            ql_value = reg_table[reg] % (1 << reg.getBitSize())

            triton_value = tt.getConcreteRegisterValue(reg)

            if ql_value != triton_value:
                # self._tt.concretizeRegister(reg)
                self._tt.setConcreteRegisterValue(reg, ql_value)
        except Exception:
            pass
            # self._logger.error(f"GerReg: {reg}")

    def __concrete_mem_hook(self, tt, mem) -> None:
        if self.__mem_hook_lock:
            return

        addr = mem.getAddress()
        size = mem.getSize()

        if self._alloc_addr is not None:
            index = addr - self._alloc_addr
            if (
                len(self.__sym_vars) <= index
                and addr < self._alloc_addr + self._alloc_size
            ):
                self.__new_sym_var(index)

        try:
            ql_value = self._ql.mem.read(addr, size)
        except Exception:
            self._logger.error(f"Memory read error on {hex(addr)} with a len of {size}")
            return

        triton_value = tt.getConcreteMemoryAreaValue(addr, size)

        # If qiling and triton mem cells are not equal, synch Triton with
        # the context of qiling
        if ql_value != triton_value:
            # tt.concretizeMemory(mem)
            tt.setConcreteMemoryAreaValue(addr, ql_value)
