import time

from emulation.runner.abi.abi import AbstractAbi
from emulation.runner.base import BaseRunner
from emulation.runner.injector.injector import AbstractInjector
from utils.data import Payload


class TimerRunner(BaseRunner):
    def __init__(self,
                 cmd,
                 rootfs,
                 snapshot,
                 abi: AbstractAbi,
                 injector: AbstractInjector,
                 iden=None):
        super().__init__(cmd, rootfs, snapshot, abi, injector, iden)

        self.time = None

    def run(self, payload: Payload):
        start_time = time.time()
        super().run(payload)
        payload.time = time.time() - start_time
