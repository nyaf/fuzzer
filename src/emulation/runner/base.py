from functools import partial
from typing import List, Optional

import unicorn
from qiling import exception

from constants import ARCH, Bug
from emulation.engine import Engine
from emulation.runner.abi import (AbstractAbi, ARM32Abi, ARM64Abi, X86Abi,
                                  X8664Abi)
from emulation.runner.config import RunnerConfig
from emulation.runner.injector.config import InjectionConfig
from emulation.runner.injector.function import FunctionInjector
from emulation.runner.injector.injector import AbstractInjector
from emulation.runner.sanitizer import (AddressSanitizer, MemorySanitizer,
                                        PosixAddressSanitizer)
from utils.data import Payload
from utils.exceptions import (DoubleFreeDetected, InvalidReadDetected,
                              InvalidWriteDetected)
from utils.log import Logger
from utils.stdio import NullOutStream


class BaseRunner(Engine):
    def __init__(self, config: RunnerConfig, iden: Optional[int] = None):
        super().__init__(config.cmd, config.rootfs)

        self._logger = Logger(type(self).__name__)
        self._snapshot = config.snapshot
        self._id = iden
        self._ql.os.stdout = NullOutStream()

        self._abi: AbstractAbi = self._create_abi(config.arch)
        self._injector: AbstractInjector = self._create_injetor(config.injection)

        self._alloc_addr = None
        self._alloc_size = 0
        self._after_inject = None
        # self.__ql_stop_hook = None
        self.__stop_address = None
        self.__start_address = None

        # After the restore, the base_address can be changed
        self._restore(self._snapshot)
        self._set_addr(config.cmd)

        if config.stop_address is not None:
            self.stop_address = config.stop_address

        if config.start_address is None:
            self.start_address = self.entry_point

        else:
            self.start_address = config.start_address

        self.__bug = Bug.NONE

        self._ql.hook_mem_invalid(partial(self.__mem_bug_detect, Bug.MEM_INVALID))
        self._ql.hook_mem_unmapped(partial(self.__mem_bug_detect, Bug.MEM_UNMAPPED))
        self._ql.hook_mem_read_invalid(partial(self.__mem_bug_detect, Bug.READ_INVALID))
        self._ql.hook_mem_write_invalid(
            partial(self.__mem_bug_detect, Bug.WRITE_INVALID)
        )
        self._ql.hook_mem_fetch_invalid(
            partial(self.__mem_bug_detect, Bug.FETCH_INVALID)
        )

        self._attach_sanitizers()
        self._injector.setup(self)
        self._abi.setup(self)

    def _attach_sanitizers(self) -> None:
        self._memory_sanitizer = MemorySanitizer(self._ql.mem)

        try:
            self._address_sanitizer = AddressSanitizer(self._ql.os.heap)
        except Exception:
            self._address_sanitizer = PosixAddressSanitizer(self._ql, self)

    def _create_abi(self, arch: ARCH) -> AbstractAbi:
        if arch == ARCH.X86_64:
            return X8664Abi()

        elif arch == ARCH.X86_32:
            return X86Abi()

        elif arch == ARCH.ARM64:
            return ARM64Abi()

        elif arch == ARCH.ARM32:
            return ARM32Abi()

        else:
            raise ValueError(f"Bad arch ({arch}) on runner {self._id}")

    def _create_injetor(self, injection_config: InjectionConfig) -> AbstractInjector:
        if injection_config.method == "function":
            return FunctionInjector(injection_config)

        else:
            raise ValueError(
                f"Bad injection method ({injection_config.method}) on runner {self._id}"
            )

    @property
    def start_address(self) -> int:
        return self.__start_address

    @start_address.setter
    def start_address(self, value: int):
        if value is None:
            self.__start_address = self.base_addr + self.entry_point

        elif value >= 0:
            self.__start_address = self.base_addr + value

    @property
    def stop_address(self) -> List[int]:
        return self.__stop_address

    @stop_address.setter
    def stop_address(self, values: List[int]):
        for value in values:
            if value >= 0:
                self.__ql_stop_hook = self._ql.hook_address(
                    self.__stop_hook, self.base_addr + value
                )

    def run(self, payload: Payload):
        self.__bug = Bug.NONE
        self._restore(self._snapshot)
        self._injector.inject(self, payload)

        self._logger.info(f"Running with payload: {payload.data}")

        try:
            super().run(begin=self._ql.arch.regs.arch_pc)
        except exception.QlMemoryMappedError:
            self.__mem_bug_detect(Bug.EXCEPTION)
            self.__bug = Bug.EXCEPTION

        except unicorn.unicorn.UcError:
            # TODO: Identify the .errno
            self.__mem_bug_detect(Bug.EXCEPTION)
            self.__bug = Bug.EXCEPTION

        except InvalidReadDetected:
            self.__bug = Bug.READ_INVALID

        except InvalidWriteDetected:
            self.__bug = Bug.WRITE_INVALID

        except DoubleFreeDetected:
            self.__bug = Bug.DOUBLE_FREE

        payload.error = self.__bug
        payload.address = self._ql.arch.regs.arch_pc - self.base_addr

    def clean(self):
        pass

    def __stop_hook(self, ql):
        self._logger.log(f"Stopping on {hex(ql.arch.regs.arch_pc)}")
        self.stop()

    def __mem_bug_detect(self, bug, *args, **kwargs):
        self.__bug = bug
        self.stop()

    def __get_map_size(self, addr) -> int:
        mem_map = self._ql.mem.get_mapinfo()

        end_map = None
        for mem in mem_map:
            if addr == mem[0]:
                end_map = mem[1]
                break

        if end_map is None:
            # maybe works
            return 4 * 1024

        return end_map - addr

    def _alloc_mem(self, size) -> int:
        if self._alloc_size < size:
            if self._alloc_addr is not None:
                self._ql.mem.unmap(self._alloc_addr, self._alloc_size)

            self._alloc_addr = self._ql.mem.map_anywhere(size, minaddr=64)
            self._alloc_size = self.__get_map_size(self._alloc_addr)

        return self._alloc_addr
