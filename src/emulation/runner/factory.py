from copy import deepcopy
from typing import Optional, Type

from emulation.runner.base import BaseRunner
from emulation.runner.config import RunnerConfig
from emulation.runner.coverage import CoverageRunner
from emulation.runner.symbolic import SymbolicRunner
from emulation.runner.timer import TimerRunner
from utils import exceptions


class RunnerFactory:
    """
    Factory for Runners. This class is used to create multiple runners with
    the same configuration.
    """

    def __init__(self, config: RunnerConfig) -> None:
        self._counter: int = 0
        self.config: RunnerConfig = config

    def _get_runner_class(self, trace: str) -> Type[BaseRunner]:
        RunnerClass: Type[BaseRunner]

        if trace is None:
            RunnerClass = BaseRunner

        elif trace == "coverage":
            RunnerClass = CoverageRunner

        elif trace == "concolic":
            RunnerClass = SymbolicRunner

        elif trace == "timer":
            RunnerClass = TimerRunner

        else:
            raise exceptions.ArgumentException(f"Bad argument 'trace': {trace}")

        return RunnerClass

    def create(
        self, trace: Optional[str] = None, timeout: Optional[int] = None
    ) -> BaseRunner:
        """
        Create a runner with the factory configuration.

        Args:
            trace (Optional[str]): The trace type of the new runner.
            timeout (Optional[int]): Override the factory timeout to the runner.
            return (Runner): The new Runner object.
        """
        # Check the tace or set default
        config: RunnerConfig

        # Check the timeout or set to None
        if timeout is not None:
            config = deepcopy(self.config)

            config.timeout = timeout if timeout > 0 else None

        else:
            config = self.config

        # Create the runner
        RunnerClass: Type[BaseRunner] = self._get_runner_class(trace)
        runner = RunnerClass(config, self._counter)

        # Increase the id counter
        self._counter += 1

        return runner
