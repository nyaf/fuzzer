from dataclasses import dataclass
from typing import Any, Dict, List, Optional

from qiling import Qiling
from qiling.cc import QlCC, arm, intel, mips, ppc, riscv
from qiling.const import QL_ARCH, QL_INTERCEPT
from qiling.os.const import POINTER, SIZE_T
from unicorn import UC_PROT_ALL

from utils.exceptions import (DoubleFreeDetected, InvalidReadDetected,
                              InvalidWriteDetected)
from utils.log import Logger


class BaseAddressSanitizer:
    PADDING_SIZE: int = 4
    PERMS: int = UC_PROT_ALL

    @staticmethod
    def _mem_protect(ql: Qiling, start: int, size: int) -> None:
        ql.mem.protect(start, start + size, BaseAddressSanitizer.PERMS)

    @staticmethod
    def _hook_read_error(
        ql: Qiling, access: int, address: int, size: int, value: int
    ) -> None:
        raise InvalidReadDetected(f"{hex(address)}:{size} <- {value}")

    @staticmethod
    def _hook_write_error(
        ql: Qiling, access: int, address: int, size: int, value: int
    ) -> None:
        raise InvalidWriteDetected(f"{hex(address)}:{size} -> {value}")

    @staticmethod
    def mem_protect(ql: Qiling, addr: int, size: int) -> None:
        ql.hook_mem_read(
            BaseAddressSanitizer._hook_read_error, begin=addr, end=addr + size - 1
        )

        ql.hook_mem_write(
            BaseAddressSanitizer._hook_write_error, begin=addr, end=addr + size - 1
        )

    @staticmethod
    def _pad_protect(ql: Qiling, addr: int, size: int, logger) -> None:
        BaseAddressSanitizer.mem_protect(
            ql,
            addr,
            BaseAddressSanitizer.PADDING_SIZE,
        )

        logger.log(f"Protecting {hex(addr)}:{BaseAddressSanitizer.PADDING_SIZE}")
        BaseAddressSanitizer.mem_protect(
            ql,
            addr + size - BaseAddressSanitizer.PADDING_SIZE,
            BaseAddressSanitizer.PADDING_SIZE,
        )

    @staticmethod
    def _add_pad(size: int) -> int:
        return size + BaseAddressSanitizer.PADDING_SIZE * 2

    @staticmethod
    def _shift_pad(address: int) -> int:
        return address + BaseAddressSanitizer.PADDING_SIZE


@dataclass()
class Alloc:
    start: int
    end: int
    free: bool = False


class PosixAddressSanitizer(BaseAddressSanitizer):
    def __init__(self, ql: Qiling, runner) -> None:
        self._allocs: List[Alloc] = []
        self._tmp_addr: Optional[int] = None
        self._last_malloc_size: int = 0

        ql.hook_address(self._attatch, runner.start_address)

        self._cc: QlCC = {
            QL_ARCH.X86: intel.cdecl,
            QL_ARCH.X8664: intel.amd64,
            QL_ARCH.ARM: arm.aarch32,
            QL_ARCH.ARM64: arm.aarch64,
            QL_ARCH.MIPS: mips.mipso32,
            QL_ARCH.RISCV: riscv.riscv,
            QL_ARCH.RISCV64: riscv.riscv,
            QL_ARCH.PPC: ppc.ppc,
        }[ql.arch.type](ql.arch)

        self._logger = Logger(type(self).__name__)
        self._logger.success(f"Initialized. Hook on: {hex(runner.start_address)}")

    def _attatch(self, ql: Qiling) -> None:
        ql.os.set_api("free", self._free_hook, QL_INTERCEPT.CALL)
        ql.os.set_api("malloc", self._malloc_enter_hook, QL_INTERCEPT.ENTER)
        ql.os.set_api("malloc", self._malloc_exit_hook, QL_INTERCEPT.EXIT)

        self._logger.success("Attached")

    def _find_chunck_range(self, address: int) -> Optional[int]:
        for alloc in self._allocs:
            if alloc.start <= address <= alloc.end:
                return alloc

        return None

    def _free_hook(self, ql: Qiling) -> None:
        self._logger.log("free hook")

        params: Dict = ql.os.resolve_fcall_params({"ptr": POINTER})

        alloc: Optional[Alloc] = self._find_chunck_range(params["ptr"])

        if alloc.free:
            raise DoubleFreeDetected(str(hex(params["ptr"])))

        alloc.free = True

        self.mem_protect(ql, alloc.start, alloc.end - alloc.start)

    @staticmethod
    def _solve_malloc_params(ql: Qiling) -> Dict:
        return ql.os.resolve_fcall_params({"size": SIZE_T})

    def _malloc_enter_hook(self, ql: Qiling) -> None:
        self._logger.log("malloc enter hook")
        params = self._solve_malloc_params(ql)

        # self._logger.log(f"Old size: {params['size']}")
        params["size"] = self._add_pad(params["size"])
        # self._logger.log(f"New size: {params['size']}")

        self._last_malloc_size = params["size"]

        return params

    def _malloc_exit_hook(self, ql: Qiling) -> None:
        self._logger.log("malloc exit hook")

        retval: int = self._cc.getReturnValue()

        # self._logger.log(f"Return value: {hex(retval)}")

        if retval == 0:
            return retval

        self._pad_protect(ql, retval, self._last_malloc_size, self._logger)

        self._allocs.append(Alloc(retval, retval + self._last_malloc_size))

        retval = self._shift_pad(retval)
        # self._logger.log(f"New return value: {hex(retval)}")

        self._cc.setReturnValue(retval)
        self._last_malloc_size = 0


class Wrapper:
    def __init__(self, wrappee: Any) -> None:
        self.wrappee: Any = wrappee

    def __getattr__(self, attr):
        return getattr(self.wrappee, attr)


class AddressSanitizer(Wrapper):
    PADDING_SIZE: int = 4
    PERMS: int = UC_PROT_ALL

    def free(self, addr: int) -> bool:
        if not self.wrappee.free(addr):
            # TODO: Custom exception
            raise DoubleFreeDetected()

        chunk_size = self.wrappee.size(addr)
        self.wrappee.ql.mem.protect(addr, chunk_size, self.PERMS)

        return True

    def alloc(self, size: int) -> int:
        real_size: int = size + self.PADDING_SIZE * 2

        address: int = self.wrappee.alloc(real_size)

        if address == 0:
            return 0

        self.wrappee.ql.mem.protect(address, self.PADDING_SIZE, self.PERMS)
        self.wrappee.ql.mem.protect(
            address + self.PADDING_SIZE + size, self.PADDING_SIZE, self.PERMS
        )

        return address + self.PADDING_SIZE


class MemorySanitizer(Wrapper):
    READ_PROT: int = 1
    WRITE_PROT: int = 2

    def _check_prot(self, addr: int, size: int, perms: int) -> bool:
        for map in self.wrappee.map_info:
            # Check intesection and permissions
            if addr <= map[1] and addr + size >= map[0] and (map[3] & perms) != 0:
                return True

        return False

    def write(self, addr: int, data: bytes) -> None:
        if self._check_prot(addr, len(data), self.WRITE_PROT):
            raise InvalidWriteDetected

        return self.wrappee.write(addr, data)

    def read(self, addr: int, size: int) -> bytearray:
        if self._check_prot(addr, size, self.READ_PROT):
            raise InvalidReadDetected()

        return self.wrappee.read(addr, size)
