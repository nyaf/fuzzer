from os.path import basename

import lief
from qiling import Qiling
from qiling.const import QL_VERBOSE

import constants as const
from utils.log import Logger


class Engine:
    """
    Basic class for emulation.

    ...

    Attributes
    ----------
    ql : Qiling
        quiling instance
    rootfs_path : str
        path to the rootfs directory
    entry_point : bool
        binary entry point
    timeout : int
        Execution timeout in microseconds

    Methods
    -------
    def run(self, begin=None, end=None):
        Runs the binary from 'begin' to end or the 'end'.
        If begin == None, starts normally.
        If end == None, run until the program finish.
    """

    def __init__(self, path, rootfs):
        if const.DEBUG:
            verb = QL_VERBOSE.DEBUG
        else:
            verb = 0

        self._ql = Qiling(path, rootfs, verbose=verb)
        self._set_addr(path)
        self.rootfs_path = rootfs
        self.timeout = None

        binary = lief.parse(path[0])
        self.entry_point = binary.header.entrypoint

        self._logger = Logger(type(self).__name__)

    def run(self, begin=None, end=None):
        """
        Runs the binary from 'begin' to end or the 'end'.
        If begin == None, starts normally.
        If end == None, run until the program finish.


        Parameters
        ----------
        begin : int, optional
            Emulation start address
        end : int, optional
            Emulation stop address

        Returns
        -------
        None
        """

        self._logger.log(f"Running from {hex(begin or 0)} to {hex(end or 0)}")

        if self.timeout is not None:
            self._ql.run(begin, end=end, timeout=self.timeout)
        else:
            self._ql.run(begin, end=end)

    def stop(self):
        """Stops the emulation"""
        self._ql.emu_stop()

    def _restore(self, sample):
        self._logger.log(f"Restoring snapshot from: {sample}")
        self._ql.restore(snapshot=sample)

    def _set_addr(self, path):
        base_path = basename(path[0])
        mem_map = self._ql.mem.get_mapinfo()

        match_maps = []

        for mem in mem_map:
            if base_path in mem[3] or (mem[4] is not None and base_path in mem[4]):
                match_maps.append(mem)

        if len(match_maps) == 0:
            raise const.EXEPT.CANT_FIND_BASE_ADDRESS

        self.base_addr = min([int(m[0]) for m in match_maps])
        self.end_addr = max([int(m[1]) for m in match_maps])

        try:
            self._logger.log(f"New base address: {hex(self.base_addr)}")
        except Exception:
            pass

        # TODO: On arm32, the map correspond with the binary address, leading
        # to a bad offset, on windows happens the same
