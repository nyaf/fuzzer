import pickle
from enum import IntEnum
from typing import Any, Optional

import zmq


class IpcSockType(IntEnum):
    PUSH: int = zmq.PUSH
    PULL: int = zmq.PULL
    SUB: int = zmq.SUB
    REP: int = zmq.REP


class IpConnection:
    def __init__(self, sock_type: IpcSockType) -> None:
        self.sock_type: IpcSockType = sock_type
        self.ready: bool = False
        self.socket: zmq.sugar.socket.Socket
        self._poller: Optional[zmq.Puller] = None
        self.port: int = -1

    def _create_puller(self) -> None:
        """Create a Poller to recv with timeout."""
        self._poller = zmq.Poller()
        self._poller.register(self.socket, zmq.POLLIN)

    def bind(self) -> int:
        """
        Bind a communication to a port.

        Return (int): The port were was binded.
        """
        if self.ready:
            return self.port

        context = zmq.Context()
        self.socket = context.socket(int(self.sock_type))

        try:
            # Bind to a random port on the localhost
            self.port = self.socket.bind_to_random_port("tcp://127.0.0.1")
        except zmq.ZMQError as error:
            raise ConnectionError(error) from error

        self._create_puller()
        self.ready = True
        return self.port

    def connect(self, connection_id: int) -> None:
        """Connect"""
        if self.ready:
            return

        context = zmq.Context()
        self.socket = context.socket(int(self.sock_type))

        try:
            # Connect
            self.socket.connect(f"tcp://127.0.0.1:{connection_id}")
        except zmq.ZMQError as error:
            raise ConnectionError(error) from error

        self._create_puller()
        self.ready = True

    def send(self, element: Any, block: bool = True) -> bool:
        """
        Send a element throw the connection (blocking).

        return True if send success.
        """
        if not self.ready:
            raise BrokenPipeError

        if block:
            try:
                self.socket.send(pickle.dumps(element))
            except zmq.ZMQError as error:
                raise ConnectionError(error) from error
        else:
            try:
                self.socket.send(pickle.dumps(element), zmq.NOBLOCK)
            except zmq.ZMQError:
                return False

        return True

    def recv(self, timeout: int = 0) -> Any:
        """
        Receive a element from the connection (blocking).

        Args:
            timeout (int): Amount of milliseconds to wait a message (0 for blocking)
        """
        if not self.ready:
            raise BrokenPipeError

        packet: Any = None

        try:
            # If timeout is set and wait but the timeout is reached, return None
            if timeout != 0 and not self._poller.poll(timeout):
                return None

            packet = pickle.loads(self.socket.recv())
        except zmq.ZMQError as error:
            raise ConnectionError(error)

        return packet

    def close(self) -> None:
        self.socket.close()
