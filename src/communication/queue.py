from abc import ABC, abstractmethod
from queue import Queue
from threading import Lock, Thread
from time import sleep
from typing import Any, Union

from communication.connection import IpConnection, IpcSockType


class AbstractQueue(ABC):
    @abstractmethod
    def get(self) -> Any:
        """Get a element form the storage (blocking)."""

    @abstractmethod
    def put(self, element: Any) -> None:
        """Put a element in the storage (blocking)."""


class IpQueue:
    """
    Inter Process Queue.
    Queue to communicate processes.

    Args:
        queue (AbstractQueue): Class to instantiate the queue.
        send_enable (boolean): If true, forward packets to an output connection.
        recv_enable (boolean): If true, receive packets from an input connection to
            the queue.

    Attributes:
        put_conn_id (int): Id of the IpConnection where send packets.
        get_conn_id (int): Id of the IpConnection where receive packets (if is_proxy).
    """

    def __init__(
        self,
        queue: AbstractQueue = Queue,
        send_enable: bool = False,
        recv_enable: bool = True,
    ) -> None:
        self.get_conn_id: Union[int, None] = None
        self.put_conn_id: Union[int, None] = None
        self.send_enable: bool = send_enable
        self.recv_enable: bool = recv_enable
        self._pull_connection: Union[IpConnection, None] = None
        self._push_connection: Union[IpConnection, None] = None
        self._threads: list[Thread] = []
        self._len: int = 0
        self.counter: int = 0
        self._len_lock = Lock()

        # Init the queue
        self._queue: AbstractQueue = queue()

        # Bind the connections
        self._init_connections()

        # Init recv and send threads
        self._init_threads()

    def _len_update(self, amount: int) -> None:
        with self._len_lock:
            self._len += amount
            if amount > 0:
                self.counter += amount

    def _init_connections(self) -> None:
        """Init the IpConnections"""
        if self.recv_enable:
            self._pull_connection = IpConnection(IpcSockType.PULL)
            self.put_conn_id = self._pull_connection.bind()

        if self.send_enable:
            self._push_connection = IpConnection(IpcSockType.PUSH)
            try:
                self.get_conn_id = self._push_connection.bind()
            except ConnectionError as error:
                # If fail, close the other connection
                if self._pull_connection is not None:
                    self._pull_connection.close()

                raise error

    def _recv_loop(self) -> None:
        """Infinite loop to forward packets from the IpConnection to the queue."""
        while True:
            # This will block
            element: Any = self._pull_connection.recv()

            # Put in the queue
            self._queue.put(element)
            self._len_update(1)

    def _send_loop(self) -> None:
        """Infinite loop to forward packets from the queue to the IpConnection."""
        while True:
            # Get a element from the queue (bloacking)
            element: Any = self._queue.get()

            # Send it (blocking)
            if self._push_connection.send(element, False):
                self._len_update(-1)
            else:
                self._queue.put(element)
                sleep(0.1)

    def _init_threads(self) -> None:
        """Init the send and recv threads."""
        if self.recv_enable:
            # Init recv thread
            thread = Thread(target=self._recv_loop, daemon=True)
            thread.start()
            self._threads.append(thread)

        if self.send_enable:
            # Init send thread
            thread = Thread(target=self._send_loop, daemon=True)
            thread.start()
            self._threads.append(thread)

    def __len__(self) -> int:
        """Get the amount of elements in the queue."""
        return self._len

    def empty(self) -> bool:
        """Retrurn True if the queue is empty."""
        return len(self) == 0

    def get(self) -> Any:
        """
        Get an element from the queue. Blocking.

        Args:
            return (Any): The element from the queue.
        """
        element: Any = self._queue.get()
        self._len_update(-1)
        return element

    def put(self, element: Any) -> None:
        """
        Put an element in the queue. Blocking.

        Args:
            element (Any): The element to put in the queue.
        """
        self._queue.put(element)
        self._len_update(1)

    def __del__(self) -> None:
        if self._push_connection is not None:
            self._push_connection.close()

        if self._pull_connection is not None:
            self._pull_connection.close()
