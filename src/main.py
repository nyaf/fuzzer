#!/usr/bin/env python
# -*- coding: utf-8 -*-

import multiprocessing
import os
import signal
import sys
import threading

from communication.queue import IpQueue
from constants import FUTTER, USAGE
from frontend.server import FuzzerTUI
from fuzzing import FuzzerFactory
from fuzzing.config import FuzzerConfig
from utils.log import Logger

exit_event = threading.Event()


def exit_handler(_, __) -> None:
    """Handle the signal to exit."""
    exit_event.set()


def print_usage() -> None:
    print(USAGE)


def parse_args() -> FuzzerConfig:
    """Parse the arguments file and convert to dict."""
    # Check the arguments
    if len(sys.argv) != 3:
        print_usage()
        sys.exit(1)

    return FuzzerConfig(sys.argv[1], sys.argv[2])


def main() -> None:
    """Where the magic happends"""
    # Store the original signal handler
    original_signal_handler = signal.getsignal(signal.SIGINT)
    # Disable SIGINT (ctrl + c) signal until initialization finish
    signal.signal(signal.SIGINT, signal.SIG_IGN)

    # if os.getenv("DEBUG") is not None:
    log_queue = IpQueue(send_enable=True, recv_enable=True)

    os.environ["LOG_PUT_IPC_ID"] = str(log_queue.put_conn_id)
    os.environ["LOG_GET_IPC_ID"] = str(log_queue.get_conn_id)
    os.environ["LOG_FILE_PATH"] = "/root/data/logs"

    logger = Logger("NYAF")

    # Enable SIGINT and set the handler
    signal.signal(signal.SIGINT, exit_handler)
    sys.stderr = open(os.devnull, "w")

    try:
        # Parse the config
        config: FuzzerConfig = parse_args()

        # Create the report and logs queue
        reports_queue = IpQueue(send_enable=True, recv_enable=True)

        # Init the UI
        tui = FuzzerTUI(reports_queue.get_conn_id)
        tui.start()

        # Start fuzzing
        fuzzer = FuzzerFactory.new_fuzzer(config, reports_queue.put_conn_id)
        fuzzer.fuzz()

        # Wait until SIGINT
        exit_event.wait()

        # Set the original handler
        signal.signal(signal.SIGINT, original_signal_handler)

        # Stop the fuzzer
        fuzzer.stop()

        # Stop the UI
        tui.stop()

        # Bye
        logger.success(FUTTER)
        sys.exit()

    except Exception as exception:
        logger.error(f"Exit with error: {exception}")
        raise exception


if __name__ == "__main__":
    # Change the default (fork in linux)
    multiprocessing.set_start_method("spawn")

    main()
