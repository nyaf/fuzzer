class Pipe:
    def __init__(self):
        self.buffer = b''

    def read(self, n):
        ret = self.buffer[:n]
        self.buffer = self.buffer[n:]
        return ret

    def readline(self, end=b'\n'):
        ret = b''
        while True:
            char = self.read(1)
            ret += char
            if char == end:
                break
        return ret

    def write(self, string):
        self.buffer += string
        return len(string)

    def clean(self):
        self.buffer = b''


class NullOutStream:
    """Null out-stream, may be used to disregard process output.
    """
    def write(self, s: bytes) -> int:
        return len(s)

    def flush(self) -> None:
        pass

    def writable(self) -> bool:
        return True
