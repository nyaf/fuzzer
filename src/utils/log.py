import inspect
import os
from typing import Optional

from communication import IpConnection, IpcSockType
from utils.data import Payload


def parse_payload_data(payload: Payload) -> bytes:
    if len(payload) > 60:
        ret = payload.data[:56] + b" [...]"
    else:
        ret = payload.data

    return ret


class bcolors:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


class Logger:
    def __init__(self, name: Optional[str]) -> None:
        self.__name: str
        if name is not None:
            self.__name = name

        else:
            stack = inspect.stack()
            self.__name = stack[1][0].f_locals["self"].__class__.__name__

        sep = 20 - len(name)
        self.__sep_beg = " " * (sep // 2)
        self.__sep_end = " " * (sep - len(self.__sep_beg))

        self._log_connection: Optional[IpConnection] = None

        log_ipc_id: Optional[str] = os.getenv("LOG_PUT_IPC_ID")

        if log_ipc_id is not None:
            self._log_connection = IpConnection(IpcSockType.PUSH)
            self._log_connection.connect(int(log_ipc_id))

    def __print_unformat(self, line: str) -> None:
        if self._log_connection is not None:
            self._log_connection.send(line)

    def __print(self, msg, color):
        line: str = f"[{self.__sep_beg}{self.__name}{self.__sep_end}] {msg}"

        self.__print_unformat(line)

    def title(self, msg):
        self.__print_unformat(msg)

    def info(self, msg):
        self.__print(msg, bcolors.OKBLUE)

    def error(self, msg):
        self.__print(msg, bcolors.FAIL)

    def success(self, msg):
        self.__print(msg, bcolors.OKGREEN)

    def log(self, msg):
        self.__print(msg, "")
