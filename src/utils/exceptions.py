class RunningException(Exception):
    """Exception running the emulation."""


class ArgumentException(Exception):
    """Invalid argument given."""


class BaseAddressException(Exception):
    """Bad base address of the emulation."""


class InvalidArch(Exception):
    """Invalid or not supported architecture."""


class UnreachedHook(Exception):
    """The runner can't reach the hook."""


class DoubleFreeDetected(Exception):
    """A double free has been detected."""


class InvalidReadDetected(Exception):
    """A read to a aprotected area has been detected."""


class InvalidWriteDetected(Exception):
    """A write to a aprotected area has been detected."""
