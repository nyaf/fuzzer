from typing import Any, Dict


class Config:
    def _parse_config(
        self,
        name: str,
        document: Dict,
        default: Any = None,
        arg_type: Any = None,
        required: bool = True,
    ) -> None:
        """
        Parse the configuration argument. Gets a name, a document and return the value.

        Args:
            name (str): Key of the argument.
            document (Dict): Dict where to get the argument.
            default (Any): Value to use if the key is not in the document.
            arg_type (Any): Expected type of the argument (non if not check).
            required (bool): Argument is required or not.
        """
        result: Any = None

        if name not in document:
            if required:
                raise AttributeError(f"'{name}' required and not in the config.")

            else:
                result = default

        else:
            if arg_type is None or isinstance(document[name], arg_type):
                result = document[name]

            elif arg_type is not None and not isinstance(document[name], arg_type):
                raise AttributeError(
                    f"Bad type on '{name}' argument. Expected: {type}. Actual: {arg_type}"
                )

        return result
