from typing import Optional


class Payload:
    def __init__(
        self, data: bytes = b"", file_path: Optional[str] = None, bound: int = 0
    ) -> None:
        self.bound: int = bound
        self.coverage: dict = {}
        self.jump_count: int = 0
        self.error: int = 0
        self.time: int = 0
        self.priority: int = 0
        self.alternatives: ["Payload"] = []
        self.address: int = 0

        if file_path is None:
            self.data: bytes = data
        else:
            with open(file_path, "rb") as file_handler:
                self.data = file_handler.read()

    def __len__(self) -> int:
        return len(self.data)

    def __lt__(self, other) -> bool:
        if self.priority != other.priority:
            return self.priority > other.priority

        if self.jump_count != other.jump_count:
            return self.jump_count > other.jump_count

        return len(self.coverage) > len(other.coverage)
