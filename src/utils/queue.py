from pickle import UnpicklingError, dumps, loads
from shutil import rmtree
from tempfile import mkdtemp
from threading import Lock
from time import sleep

from queuelib import FifoDiskQueue, PriorityQueue

from utils.data import Payload


class PayloadPriorityQueue:
    """
    Persistent Priority queue of Payloads.
    """

    def __init__(self) -> None:
        self._path: str = mkdtemp()
        self._mutex = Lock()

        def queue_factory(priority):
            return FifoDiskQueue(f"{self._path}/{priority}")

        self._queue = PriorityQueue(queue_factory)

    def get(self) -> Payload:
        """
        Get a Payload from the queue. Blocking.

        Args:
            return (Payload): The Payload from the queue.
        """
        while True:
            with self._mutex:
                payload = self._queue.pop()

            if payload is None:
                sleep(0.100)
            else:
                try:
                    return loads(payload)
                except UnpicklingError:
                    pass

    def put(self, payload: Payload) -> None:
        """
        Put a Payload in the queue. Blocking.

        Args:
            payload (Payload): The Payload to put in the queue.
        """
        with self._mutex:
            self._queue.push(dumps(payload), -payload.priority)

    def empty(self) -> bool:
        """Retrurn True if the queue is empty."""
        return len(self._queue) == 0

    def qsize(self) -> int:
        """Return the size of the queue."""
        return len(self)

    def __len__(self) -> int:
        return len(self._queue)

    def __del__(self) -> None:
        rmtree(self._path)
